import json

import pandas as pd
import requests

# Load the CSV file
csv_file = "/workspace/.devcontainer/scripts/budgetposten_utf8.csv"  # File path
data = pd.read_csv(csv_file)
# data = data.iloc[:2]  # Limit the number of rows for testing

# API endpoint and headers
api_url = "https://api-qtool2.amiv.ethz.ch/api/ksts/"  # Replace with your API endpoint
headers = {
    "Authorization": "Bearer sZSkGMhlVLkozABUId_A7EJtNpbgTn0-6FbIF7P1-Uw",  # Replace with your API key/token
    "Content-Type": "application/json",
}


# Loop through the CSV rows and send requests
for index, row in data.iterrows():
    # Prepare the payload (adjust to match your API schema)
    payload = {
        "kst_number": str(row["kst_number"]),  # Ensure kst_number is a string
        "name_de": str(row["name_de"]),  # Ensure name_de is a string
        "name_en": str(row["name_en"]),  # Ensure name_en is a string
        "owner": str(row["owner"]),  # Ensure owner is a string
        "active": bool(row["active"]),  # Convert active to boolean
        "budget_plus": int(row["budget_plus"])
        * 100,  # Ensure budget_plus is an integer
        "budget_minus": int(row["budget_minus"])
        * 100,  # Ensure budget_minus is an integer
    }

    print(json.dumps(payload))

    # Send the POST request
    response = requests.post(api_url, json=payload, headers=headers)

    # Check the response status
    if response.status_code == 201:  # Adjust status code as needed (e.g., 200 or 201)
        print(f"Row {index} uploaded successfully!")
    else:
        print(f"Failed to upload row {index}. Status code: {response.status_code}")
        print(response.text)  # Print the error details if needed
