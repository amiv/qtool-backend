from sqlalchemy import create_engine, text
from sqlalchemy.exc import SQLAlchemyError

from app.core.config import Settings


def drop_database(db_url: str, database_name: str) -> None:
    """
    Drops the specified database.

    Parameters:
    db_url (str): The connection URL to the MariaDB instance without the database.
    database_name (str): The name of the database to drop.

    Raises:
    SQLAlchemyError: If there's an issue dropping the database.
    """
    # Strip the database from the connection URL to connect without selecting a DB
    base_db_url = db_url.rsplit("/", 1)[0]  # Remove the last /app part

    # Create engine without selecting the database
    engine = create_engine(base_db_url)

    try:
        with engine.connect() as connection:
            # Drop the database if it exists
            connection.execute(text(f"DROP DATABASE IF EXISTS `{database_name}`"))
            print(f"Database '{database_name}' dropped successfully.")
    except SQLAlchemyError as e:
        print(f"Error occurred while dropping the database: {e}")
    finally:
        engine.dispose()


def create_database(db_url: str, database_name: str) -> None:
    """
    Creates the specified database.

    Parameters:
    db_url (str): The connection URL to the MariaDB instance without the database.
    database_name (str): The name of the database to create.

    Raises:
    SQLAlchemyError: If there's an issue creating the database.
    """
    # Strip the database from the connection URL to connect without selecting a DB
    base_db_url = db_url.rsplit("/", 1)[0]

    # Create engine without selecting the database
    engine = create_engine(base_db_url)

    try:
        with engine.connect() as connection:
            # Create the database
            connection.execute(text(f"CREATE DATABASE `{database_name}`"))
            print(f"Database '{database_name}' created successfully.")
    except SQLAlchemyError as e:
        print(f"Error occurred while creating the database: {e}")
    finally:
        engine.dispose()


if __name__ == "__main__":
    # Connection string to the MariaDB without the database selected
    db_url = Settings.SQLALCHEMY_DATABASE_URI

    # Extract the database name and drop it
    database_name = db_url.split("/")[-1]

    # Ask for user confirmation before proceeding
    confirmation = input(
        f"Are you sure you want to drop the database '{database_name}'? Type 'YES' to confirm: "
    )

    if confirmation == "YES":
        # Drop the database
        drop_database(db_url, database_name)
        # Re-create the database
        create_database(db_url, database_name)
    else:
        print("Database drop operation canceled.")
