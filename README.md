
# QTool Backend

This repository contains the backend for the new QTool.

## Development Setup

To start the development server, run the following command:

```bash
fastapi dev app/main.py
```

The docs are available here at http://localhost:8000/docs#/
Don't forget the refresh the docs site after changing stuff.

### Adminer Interface

The Adminer interface is automatically started at `localhost:8080` when using the development container. You can access the PostgreSQL database using the following credentials:

- **System**: PostgreSQL
- **Database**: `app`
- **User**: `postgres`
- **Password**: `qtool`

### Database Management

To manage the database schema and perform migrations, use Alembic.

#### Create a New Database Migration

To create a new database migration, run:

```bash
alembic revision --autogenerate -m "change message"
```

#### Apply the Latest Migration

To upgrade the database to the latest migration, run:

```bash
alembic upgrade head
```

## TODOs
- with python 3.14 uuid7 should be part of the uuid library. rebuild all of the uuid6.uuid7() calls to uuid.uuid7() this shoudn't change anything else. 