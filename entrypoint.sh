#! /bin/bash

alembic upgrade head
ls -l /app/app
ls -l /app/*/*
fastapi run --workers 4 /app/app/main.py --host 0.0.0.0
