import uuid
from datetime import datetime
from enum import Enum
from typing import Any, List, Type

from faker import Faker
from sqlalchemy import inspect as sqlalchemy_inspect
from sqlalchemy.orm import RelationshipProperty
from sqlmodel import Session, SQLModel, select

# Initialize Faker instance
fake = Faker()


# Helper function to generate a UUID or default values for fields
# Helper function to generate a UUID or default values for fields
def generate_field_value(field_type: Type[Any]):
    origin_type = getattr(field_type, "__origin__", None)
    if (
        origin_type is not None
        or hasattr(field_type, "__args__")
        and len(field_type.__args__) > 1
    ):
        # Handle Optional and other generic types
        if origin_type is Union or len(field_type.__args__) > 1:
            args = field_type.__args__
            # Assume the first type is the main type
            return generate_field_value(args[0])

    if field_type == uuid.UUID:
        return uuid.uuid4()
    elif field_type == str:
        return fake.text(max_nb_chars=30).strip().replace("\n", " ")
    elif field_type == int:
        return fake.random_int(min=1, max=100000)
    elif field_type == float:
        return fake.pyfloat(left_digits=5, right_digits=2, positive=True)
    elif field_type == datetime:
        return fake.date_time_between(start_date="-2y", end_date="now")
    elif field_type == bool:
        return fake.boolean()
    elif issubclass(field_type, Enum):  # Check if the field is an Enum
        return fake.random_element(field_type)
    elif issubclass(field_type, SQLModel):
        # For nested SQLModel types, we'll handle them separately
        return None
    else:
        return None


# Function to get or create instances of related models (for foreign key fields)
def get_existing_instance(
    session: Session, model_class: Type[SQLModel], created_instances: dict
):
    if model_class in created_instances:
        return created_instances[model_class]
    # Check if the table already has records
    statement = select(model_class)
    result = session.exec(statement).all()
    if result:
        # Randomly pick one from existing data
        created_instances[model_class] = fake.random_element(result)
    else:
        # If no records exist, generate a new one
        instance = generate_model_instance(model_class, session, created_instances)
        session.add(instance)
        session.commit()
        created_instances[model_class] = instance

    return created_instances[model_class]


# Generate data dynamically for a SQLModel instance
def generate_model_instance(
    model_class: Type[SQLModel], session: Session, created_instances: dict
):
    model_fields = (
        model_class.model_fields
    )  # Pydantic v2 uses model_fields instead of __fields__
    field_data = {}

    for field_name, field in model_fields.items():
        field_type = (
            field.annotation
        )  # Pydantic v2 uses 'annotation' instead of 'type_'
        origin_type = getattr(field_type, "__origin__", None)

        # Check if the field is a relationship or foreign key
        mapper = sqlalchemy_inspect(model_class)
        if mapper and field_name in mapper.relationships:
            relationship_prop: RelationshipProperty = mapper.relationships[field_name]
            related_model = relationship_prop.mapper.class_
            related_instance = get_existing_instance(
                session, related_model, created_instances
            )
            field_data[field_name] = related_instance
            continue

        # For foreign key fields (e.g., kst_id), ensure they reference actual related models
        if field_name.endswith("_id"):
            related_model_name = field_name[:-3].capitalize()  # Example: kst_id -> Kst
            related_model_class = globals().get(related_model_name)
            if related_model_class:
                related_instance = get_existing_instance(
                    session, related_model_class, created_instances
                )
                field_data[field_name] = related_instance.id
                continue

        # Generate value for the field
        value = generate_field_value(field_type)
        if value is not None:
            field_data[field_name] = value

    # Add common fields if they exist
    if "id" in model_fields and model_fields["id"].default_factory is not None:
        field_data["id"] = uuid.uuid4()
    if (
        "time_create" in model_fields
        and model_fields["time_create"].default_factory is not None
    ):
        field_data["time_create"] = datetime.now()
    if (
        "time_modified" in model_fields
        and model_fields["time_modified"].default_factory is not None
    ):
        field_data["time_modified"] = datetime.now()

    # Create the model instance with the generated field values
    return model_class(**field_data)


# Populate the database with random instances of the provided model classes
def populate_db(
    session: Session, model_classes: List[Type[SQLModel]], instances_per_model: int = 10
):
    created_instances = {}

    for model_class in model_classes:
        for _ in range(instances_per_model):
            model_instance = generate_model_instance(
                model_class, session, created_instances
            )
            session.add(model_instance)
            print(f"Generated {model_class.__name__} : {model_instance}")

    # Commit all changes to the database
    session.commit()


# Example usage with models
if __name__ == "__main__":
    from app.core.config import engine  # Ensure you have your DB engine configured
    from app.model import *

    # Create database tables if they don't exist
    # SQLModel.metadata.create_all(engine)

    with Session(engine) as session:
        # Populate related models first to ensure valid foreign key references
        populate_db(session, [Kst, Ledger, Address, Item], instances_per_model=5)
