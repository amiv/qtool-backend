alembic:
	echo "Running alembic"
	alembic current
	alembic upgrade head

clean:
	python3 dropDB.py
	alembic upgrade head
	python3 generator.py
