from fastapi import APIRouter

from app.api import auth
from app.api.routes import (
    addresses,
    bills,
    combined,
    creditPayments,
    evaluation,
    ezags,
    files,
    internalTransfers,
    invoices,
    items,
    ksts,
    ledgers,
    reimbursements,
    users,
)

api_router = APIRouter()
api_router.include_router(items.router, tags=["items"], prefix="/items")
api_router.include_router(invoices.router, tags=["invoices"], prefix="/invoices")
api_router.include_router(ledgers.router, tags=["ledgers"], prefix="/ledgers")
api_router.include_router(ksts.router, tags=["ksts"], prefix="/ksts")
api_router.include_router(addresses.router, tags=["addresses"], prefix="/addresses")
api_router.include_router(
    internalTransfers.router, tags=["internalTransfers"], prefix="/internalTransfers"
)
api_router.include_router(bills.router, tags=["bills"], prefix="/bills")
api_router.include_router(
    reimbursements.router, tags=["reimbursements"], prefix="/reimbursements"
)
api_router.include_router(
    creditPayments.router, tags=["creditPayments"], prefix="/creditPayments"
)
api_router.include_router(files.router, tags=["files"], prefix="/files")
api_router.include_router(auth.router, tags=["auth"])
api_router.include_router(users.router, tags=["users"], prefix="/users")
api_router.include_router(combined.router, tags=["combined"], prefix="/combined")
api_router.include_router(evaluation.router, tags=["evaluation"], prefix="/evaluation")
api_router.include_router(ezags.router, tags=["ezags"], prefix="/ezags")
