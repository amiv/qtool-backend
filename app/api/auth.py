import uuid
from typing import Annotated, Any

import requests
from authlib.integrations.starlette_client import OAuth
from fastapi import APIRouter, Depends, HTTPException, Request, status
from fastapi.security import HTTPBearer
from sqlmodel import Session
from starlette.config import Config

from app.api.routes.api_helper import read_object, read_objects
from app.core.config import engine, settings
from app.models.addresses import Address, AddressPublic, UserAddress
from app.models.ksts import Kst, KstFilter, KstsPublic
from app.models.Users import BasicUser, DbUser, DbUserPublic, User

router = APIRouter()
config = Config(".env")

# Secure OAuth configuration
oauth = OAuth(config)
oauth.register(
    name="amivapi",
    client_id=settings.OAUTH_CLIENT_ID,  # Securely fetch client ID
    authorize_url=f"https://{settings.AMIV_API_URL}/oauth",
    response_type="token",
)


@router.get("/login")
async def login(request: Request):
    """
    Redirects the caller to the OAuth2 provider for authentication using the Implicit flow.
    """
    try:
        redirect_uri = request.headers["Referer"] + "callback"
        # Validate the redirect URI if necessary to avoid open redirects
        return await oauth.amivapi.authorize_redirect(
            request, redirect_uri=redirect_uri
        )
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))


oauth2_scheme = (
    HTTPBearer()
    # OAuth2(flows=OAuthFlows(implicit=OAuthFlowImplicit( authorizationUrl=f"{settings.AMIV_API_URL}/oauth?"))) #(authorizationUrl=f"{settings.AMIV_API_URL}/oauth?",tokenUrl="token",response_type="token")
)


def basic_user_info(session: str = Depends(oauth2_scheme)) -> BasicUser:
    """
    Get basic user info by requesting session info from the api using a session token.
        {
                "_id": "5d8086bee61dce5c6bc1a8b7",
                "nethz": "cwalter",
                "legi": "19919935",
                "firstname": "Clemens",
                "lastname": "Walter",
                "email": "
        }"""
    session_token = session.credentials
    headers = {
        "Authorization": f"Bearer {session_token}",
    }
    response = requests.get(
        f"https://{settings.AMIV_API_URL}/sessions/{session_token}?embedded="
        + '{"user":1}',
        headers=headers,
    )
    # print(response.json())
    if response.status_code != 200:
        raise HTTPException(status_code=400, detail="Invalid session token2")
    js = response.json()["user"]
    js["id"] = js["_id"]
    # print(js)

    groups = get_user_groups(js["_id"], session)
    # print(settings.RIGHTS_TREASURERS, groups)
    if settings.RIGHTS_TREASURERS in groups:
        js["role"] = "quaestor"
    elif settings.RIGHTS_AUDITORS in groups:
        js["role"] = "auditor"
    basicUser = BasicUser.model_validate(js, strict=False)

    return basicUser


@router.get("/basic-user-info", response_model=BasicUser)
def get_basic_user_info(current_user: Annotated[User, Depends(basic_user_info)]):

    return current_user


def get_api_user(session: str = Depends(oauth2_scheme)) -> dict:
    """
    Get basic user info by requesting session info from the api using a session token.
    returns a user object like this:
        {
                "_id": "5d8086bee61dce5c6bc1a8b7",
                "nethz": "cwalter",
                "legi": "19919935",
                "firstname": "Clemens",
                "lastname": "Walter",
                "email": "
        }"""
    session_token = session.credentials
    headers = {
        "Authorization": f"Bearer {session_token}",
    }
    # print(session_token)
    response = requests.get(
        f"https://{settings.AMIV_API_URL}/sessions/{session_token}?embedded="
        + '{"user":1}',
        headers=headers,
    )
    if response.status_code != 200:
        raise HTTPException(status_code=400, detail="Invalid session token")
    return response.json()["user"]


def get_user_info(session: str = Depends(oauth2_scheme)) -> User:
    """
    Get user info by requesting session info from the api using a session token.
    This returns all the relevant info.
    We need to store the iban and address everything else can be requested from the api.
    returns a user object like this:
        {
                "_id": "5d8086bee61dce5c6bc1a8b7",
                "nethz": "cwalter",
                "legi": "19919935",
                "firstname": "Clemens",
                "lastname": "Walter",
                "email": "cwalter@ethz.ch",
                "gender": "male",
                "department": "itet",
                "membership": "regular",
                "rfid": "342004",
                "phone": null,
                "send_newsletter": true,
                "_updated": "2024-11-12T05:01:39Z",
                "_created": "2019-09-17T07:09:50Z",
                "_etag": "05a22148fe715f33458a0c0c3fc4002265d9b76c",
                "password_set": true
        }
    """
    session_token = session.credentials
    headers = {
        "Authorization": f"Bearer {session_token}",
    }
    # print(session_token)
    response = requests.get(
        f"https://{settings.AMIV_API_URL}/sessions/{session_token}?embedded="
        + '{"user":1}',
        headers=headers,
    )
    if response.status_code != 200:
        raise HTTPException(status_code=400, detail="Invalid session token")
    dbuser = {}
    try:
        with Session(engine) as db_session:
            dbuser_object: DbUser = db_session.get(
                DbUser, response.json()["user"]["_id"]
            )
            address = db_session.get(UserAddress, dbuser_object.address_id)
            dbuser_public: DbUserPublic = DbUserPublic.model_validate(
                dbuser_object, update={"address": address}
            )
            dbuser = dbuser_public.model_dump()
            print(dbuser)
    except Exception as e:
        # print(e)
        print("User not found in db")
        raise HTTPException(status_code=403, detail="This user isn't onboarded yet")
    dbuser.update(response.json()["user"])
    # print(dbuser)
    groups = get_user_groups(dbuser["id"], session)
    # print(groups)
    role = None
    if settings.RIGHTS_TREASURERS in groups:
        role = "quaestor"
    elif settings.RIGHTS_AUDITORS in groups:
        role = "auditor"
    user: User = User.model_validate(dbuser, update={"role": role, "groups": groups})
    # print(user)
    return user


def is_quaestor(session: str = Depends(oauth2_scheme)) -> User:
    """get a user if he is a quaestor, else raises an exception"""
    user = get_user_info(session)
    if user.role == "quaestor":
        return user
    raise HTTPException(status_code=403, detail="This requires quaestor rights")


def is_auditor(session: str = Depends(oauth2_scheme)) -> User:
    """get a user if he is a quaestor or auditor, else raises an exception"""
    user = get_user_info(session)
    if user.role == "auditor" or user.role == "quaestor":
        return user
    raise HTTPException(status_code=403, detail="This requires auditor rights")


def is_kst_responsible(session: str = Depends(oauth2_scheme)) -> User:
    """get a user if he is a kst responsible, else raises an exception.
    Quaestors are global kst responsible.
    """
    user = get_user_info(session)
    if user.role == "quaestor":
        return user
    groups = user.groups
    ksts = read_objects(Kst, 0, 100000, KstFilter(), Kst.kst_number, KstsPublic)
    for kst in ksts.items:
        if kst.owner in groups:
            return user
    raise HTTPException(status_code=403, detail="This requires kst responsible rights")


@router.get("/is-onboarded")
def is_onboarded(session: str = Depends(oauth2_scheme)):
    """
    Check if the user is onboarded.
    Return True if he is, otherwise False.
    """
    try:  # try to get the user info
        get_user_info(session)
        return True
    except HTTPException as e:
        if e.status_code == 403:
            return False
        raise e


def is_logged_in(session: str = Depends(oauth2_scheme)):
    """
    Check if the user is logged in.
    """
    try:
        get_user_info(session)
        return True
    except:
        return False


def get_user_groups(id: str, session) -> list[str]:
    """
    Get user groups by requesting session info from the api using a session token.
    This returns all the relevant info.
    We need to store the iban and address everything else can be requested from the api.
    returns a list of objects of the form:
    {
                        "_id": "5b07dde7fa5f6e0001a29de1",
                        "name": "HoPo ITET",
                        "requires_storage": true,
                        "moderator": null,
                        "_updated": "2022-09-29T14:36:20Z",
                        "_created": "2018-05-25T09:56:55Z",
                        "allow_self_enrollment": false,
                        "_etag": "a8b722bf13695a7e1213f33ef083c11a21e522d1",
                        "receive_from": [],
                        "forward_to": null,
                        "permissions": {}
    """
    headers = {
        "Authorization": f"Bearer {session.credentials}",
    }
    response = requests.get(
        f"https://{settings.AMIV_API_URL}/groupmemberships",
        # https://api.amiv.ethz.ch/groupmemberships?where=%7B%22user%22%3A%225d8086bee61dce5c6bc1a8b7%22%7D&embedded=%7B%22group%22%3A1%7D
        params={
            "where": f'{{"user":"{id}"}}',
            "embedded": '{"group":1}',
            "max_results": 50,
        },
        headers=headers,
    )
    if response.status_code != 200:
        raise HTTPException(status_code=400, detail="Invalid session token")
    # print(response.request.url)
    groups = []
    # print(response.json())
    for group in response.json()["_items"]:
        if group["group"] is not None:
            groups.append(group["group"]["_id"])
    return groups


"""
token = "token"
print(get_user_info(token))
print(is_logged_in(token))
print(get_user_groups(token))
print(is_user_part_of_group(token, "5ac9246b4db3bd0001e9f2a8"))  # dev vorstand gruppe.
"""
