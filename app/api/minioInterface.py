import os

from minio import Minio
from minio.error import S3Error


# Initialize the MinIO client
def initialize_minio_client():
    minio_client = Minio(
        "minio:9000",  # MinIO server URL
        access_key="minio",  # Access key as defined in docker-compose
        secret_key="minio123",  # Secret key as defined in docker-compose
        secure=False,  # Use secure=True if using https
    )
    return minio_client


# Create a bucket if it doesn't exist
def create_bucket(client: Minio, bucket_name: str):
    found = client.bucket_exists(bucket_name)
    if not found:
        client.make_bucket(bucket_name)
    else:
        print(f"Bucket '{bucket_name}' already exists")


# Upload a file to the bucket
def upload_file(client: Minio, bucket_name: str, file_path: str, object_name: str):
    try:
        client.fput_object(bucket_name, object_name, file_path)
        print(f"File '{file_path}' uploaded successfully as '{object_name}'")
    except S3Error as exc:
        print(f"Error uploading file: {exc}")


# Download a file from the bucket
def download_file(
    client: Minio, bucket_name: str, object_name: str, download_path: str
):
    try:

        client.fget_object(bucket_name, object_name, download_path)
        print(f"File '{object_name}' downloaded successfully to '{download_path}'")
    except S3Error as exc:
        print(f"Error downloading file: {exc}")


if __name__ == "__main__":
    # Initialize the MinIO client
    client = initialize_minio_client()

    # Create a bucket (if it doesn't exist)
    bucket_name = "my-bucket"
    create_bucket(client, bucket_name)

    # Upload a file
    print(os.listdir("./"))
    file_to_upload = "./generator.py"
    object_name = "generator.py"
    upload_file(client, bucket_name, file_to_upload, object_name)

    # Download the file
    download_file_path = "download.txt"
    download_file(client, bucket_name, object_name, download_file_path)
