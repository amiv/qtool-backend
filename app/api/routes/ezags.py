import uuid
from datetime import datetime
from typing import Annotated, Any, Dict, Optional, Type, Union, get_args, get_origin

import uuid6
from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from pydantic import BaseModel, ConfigDict
from sqlalchemy import String, asc, desc, text
from sqlmodel import (
    Field,
    Relationship,
    Session,
    SQLModel,
    cast,
    create_engine,
    func,
    insert,
    select,
)

from app.api.auth import (
    get_user_groups,
    get_user_info,
    is_auditor,
    is_kst_responsible,
    is_quaestor,
)
from app.api.processors.xmlgenerator.xmlgenerator import create_iso20022_file
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    read_object,
    read_objects,
    updated_object,
)
from app.api.routes.files import get_file, upload_xml
from app.core.config import engine
from app.model import (
    Address,
    Creditor,
    CreditPayment,
    CreditPaymentBase,
    CreditPaymentCreate,
    CreditPaymentFilter,
    CreditPaymentPublic,
    CreditPaymentsList,
    Debitor,
    Reimbursement,
)
from app.models.addresses import AddressFilter
from app.models.bills import Bill, BillFilter
from app.models.creditor import Creditor, CreditorFilter, CreditorPublic, q_state
from app.models.creditPayments import Card
from app.models.ezags import Ezag, EzagFilter, EzagList, EzagPublic, EzagPublicList
from app.models.filter import BaseFilter
from app.models.internalTransfers import InternalTransfer
from app.models.ksts import Kst
from app.models.ledgers import Ledger
from app.models.reimbursements import ReimbursementFilter
from app.models.Users import DbUser, User

router = APIRouter()


@router.post("/generateEzag", response_model=str)
def generateEzag(
    current_user: Annotated[User, Depends(is_quaestor)],
) -> str:
    """
    Generate a ezag file for all entries that have beeen accepted and that aren't in a ezag file yet.

    """
    # get all Reimbursements that are accepted and not in a ezag file
    with Session(engine) as db_session:
        query = select(Reimbursement)
        Reimbursements_object = db_session.exec(query).all()
        # print(Reimbursements_object)
        query = (
            select(Reimbursement)
            .join(Creditor)
            .where(
                Creditor.q_check == q_state.accepted,
                Reimbursement.ezag_timestamp == None,
            )
        )

        Reimbursements_object = db_session.exec(query).all()
        print(Reimbursements_object)
        reimbursements_json = []
        for reimbursement in Reimbursements_object:
            # create a new ezag file
            j = reimbursement.model_dump()
            recipient = db_session.get(DbUser, reimbursement.recipient)

            j["recipient"] = recipient.model_dump()
            j["recipient"]["address"] = recipient.address.model_dump()
            j["creditor"] = reimbursement.creditor.model_dump()

            # print(j)
            reimbursements_json.append(j)
        iso = create_iso20022_file(reimbursements_json)
        # print(iso)
        ezag: Ezag = Ezag().model_validate(
            {
                "name": f"Msg-{datetime.now().strftime("%Y%m%d")}",
                "size": len(reimbursements_json),
                "time_create": datetime.now(),
                "time_modified": datetime.now(),
            }
        )
        print(ezag)
        db_session.add(ezag)
        db_session.commit()
        for reimbursement in Reimbursements_object:
            reimbursement.ezag_id = ezag.id
            reimbursement.ezag_timestamp = datetime.now()
            db_session.merge(reimbursement)
        db_session.commit()
        upload_xml(ezag.id, iso)

    download_link = get_file(current_user, ezag.id)
    return download_link


@router.get("/{id}", response_model=EzagPublic)
def read_Ezag(
    current_user: Annotated[User, Depends(is_quaestor)],
    id: uuid.UUID,
) -> Ezag:
    """
    Get a specific Ezag.
    """
    ezag = read_object(Ezag, id, Ezag)
    link = get_file(current_user, ezag, id)
    ret_val = EzagPublic.model_validate(ezag, update={"link": link})
    return ret_val


@router.get("/", response_model=EzagPublicList)
def read_Ezags(
    current_user: Annotated[User, Depends(is_quaestor)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: EzagFilter().create_flattened_model() = Depends(),
) -> EzagPublicList:
    """
    Get all Ezags.
    """
    return_obj: EzagList = read_objects(Ezag, page, limit, filter, Ezag.id, EzagList)
    print(return_obj)
    ezags_pub = EzagPublicList(items=[], count=return_obj.count, total=return_obj.total)
    for ezag in return_obj.items:
        link = get_file(current_user, ezag.id, 3600)
        print(ezag)
        epub = EzagPublic(
            id=ezag.id,
            name=ezag.name,
            time_create=ezag.time_create,
            link=link,
            size=ezag.size,
        )
        print(epub)
        ezags_pub.items.append(epub)
    print(ezags_pub)
    return ezags_pub
