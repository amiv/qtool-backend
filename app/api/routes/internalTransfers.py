import uuid
from typing import Annotated, Any

from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from sqlmodel import Session, create_engine, func, insert, select

from app.api.auth import get_user_info, is_auditor, is_quaestor
from app.api.processors.email.prepare_email import (
    send_internal_transfer_accepted_email,
    send_internal_transfer_declined_email,
)
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    q_fields_creditor,
    read_object,
    read_objects,
    update_creditor_name,
    updated_object,
)
from app.core.config import engine
from app.model import (
    Creditor,
    Debitor,
    InternalTransfer,
    InternalTransferBase,
    InternalTransferCreate,
    InternalTransferFilter,
    InternalTransferPublic,
    InternalTransfersList,
)
from app.models.creditor import q_state
from app.models.Users import User

router = APIRouter()


@router.get("/", response_model=InternalTransfersList)
def read_InternalTransferes(
    current_user: Annotated[User, Depends(is_auditor)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: InternalTransferFilter().create_flattened_model() = Depends(),
) -> InternalTransfersList:
    """
    retrieve all InternalTransferes, sorted by name.
    paginate the results.
    """
    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    return read_objects(
        InternalTransfer,
        page,
        limit,
        filter,
        InternalTransfer.id,
        InternalTransfersList,
    )


@router.get("/{id}", response_model=InternalTransferPublic)
def read_InternalTransfer(
    current_user: Annotated[User, Depends(get_user_info)], id: uuid.UUID
) -> InternalTransferPublic:
    """
    retrieve a single InternalTransfer by id.
    """
    ret_object = read_object(InternalTransfer, id, InternalTransferPublic)
    base_object: InternalTransfer = read_object(InternalTransfer, id, InternalTransfer)
    if not (current_user.role == "quaestor" or current_user.role == "auditor"):
        if ret_object.creditor.creator_id != current_user.id:
            if not (base_object.creditor.kst.owner in current_user.groups):
                if not (ret_object.debitor.kst.owner in current_user.groups):
                    raise HTTPException(
                        status_code=401, detail="Only admins can see this"
                    )
    return ret_object


@router.post("/", response_model=InternalTransferPublic)
def create_InternalTransfer(
    current_user: Annotated[User, Depends(get_user_info)],
    InternalTransfer_in: InternalTransferCreate,
) -> InternalTransferPublic:
    """
    create a new InternalTransfer.
    """
    q_fields_creditor(current_user, InternalTransfer_in.creditor)
    creditor = Creditor().model_validate(InternalTransfer_in.creditor)
    debitor = Debitor().model_validate(InternalTransfer_in.debitor)
    with Session(engine) as session:
        session.add(creditor)
        session.add(debitor)
        session.commit()
        session.refresh(creditor)
        session.refresh(debitor)
        internalTransfer = InternalTransfer().model_validate(
            InternalTransfer_in,
            update={
                "creditor": creditor,
                "debitor": debitor,
                "creditor_id": creditor.id,
                "debitor_id": debitor.id,
                "name": "InternalTransfer",
            },
        )
        session.add(internalTransfer)
        internalTransfer.creditor.namefunction()
        session.commit()
        session.refresh(internalTransfer)
    print(internalTransfer)
    returnInternalTransfer = InternalTransferPublic.model_validate(internalTransfer)
    return returnInternalTransfer


@router.patch("/{id}", response_model=InternalTransferPublic)
def update_InternalTransfer(
    current_user: Annotated[User, Depends(get_user_info)],
    id: uuid.UUID,
    internalTransfer_in: InternalTransferPublic,
) -> InternalTransferPublic:
    """
    update an InternalTransfer.
    """
    initial_object: InternalTransfer = read_object(
        InternalTransfer, id, InternalTransfer
    )
    print(initial_object)
    print(internalTransfer_in)
    if initial_object.creditor.q_check == q_state.accepted:
        # only quaestor can change anything
        if current_user.role != "quaestor":
            raise HTTPException(status_code=403, detail="Only quaestor can change this")
    q_fields_creditor(
        current_user, internalTransfer_in.creditor, initial_object.creditor
    )
    if (
        internalTransfer_in.creditor.q_check == q_state.accepted
        and initial_object.creditor.q_check == q_state.open
    ):
        # send accepted email
        send_internal_transfer_accepted_email(
            initial_object.id,
            initial_object.creditor.creator.nethz,
            initial_object.creditor.comment,
            initial_object.creditor.amount,
        )
    elif (
        internalTransfer_in.creditor.q_check == q_state.rejected
        and initial_object.creditor.q_check == q_state.open
    ):
        # send declined email
        send_internal_transfer_declined_email(
            initial_object.id,
            initial_object.creditor.creator.nethz,
            initial_object.creditor.qcomment,
            initial_object.creditor.amount,
        )

    if (
        initial_object.creditor.q_check == q_state.rejected
        and current_user.role != "quaestor"
    ):
        # if the user has a rejected bill it is set to open again after any change.
        internalTransfer_in.creditor.q_check = q_state.open
    updated_object(
        {"id": internalTransfer_in.debitor.id}, internalTransfer_in.debitor, Debitor
    )
    updated_object(
        {"id": internalTransfer_in.creditor.id}, internalTransfer_in.creditor, Creditor
    )
    internalTransfer = InternalTransfer().model_validate(
        internalTransfer_in,
        update={
            "creditor": internalTransfer_in.creditor,
            "debitor": internalTransfer_in.debitor,
            "creditor_id": internalTransfer_in.creditor.id,
            "debitor_id": internalTransfer_in.debitor.id,
        },
    )
    returnInternaltransfer = InternalTransferPublic.model_validate(
        updated_object({"id": id}, internalTransfer, InternalTransfer)
    )
    update_creditor_name(internalTransfer.creditor_id)
    returnInternaltransfer.creditor.name = internalTransfer.creditor.name
    return returnInternaltransfer


@router.delete("/{id}", response_model=InternalTransfer)
def delete_InternalTransfer(
    current_user: Annotated[User, Depends(is_quaestor)], id: uuid.UUID
) -> InternalTransfer:
    """
    delete an InternalTransfer.
    """
    delete_message = delete_object(InternalTransfer, id)
    # Delete the associated Creditor if it exists
    delete_object(Creditor, delete_message.creditor_id)
    # Delete the associated Debitor if it exists
    delete_object(Debitor, delete_message.debitor_id)

    return delete_message
