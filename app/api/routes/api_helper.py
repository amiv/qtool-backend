import uuid
from datetime import datetime
from typing import Any, Optional, Type, TypeVar, Union, cast

from fastapi import APIRouter, HTTPException, Request
from fastapi_filter.contrib.sqlalchemy import Filter
from sqlmodel import Session, SQLModel, func, insert, select

from app.core.auth_helper import q_field
from app.core.config import engine
from app.model import (
    AllDbModels,
    AllModelsBase,
    AllModelsCreate,
    AllModelsFilter,
    AllModelsList,
    AllModelsPublic,
)
from app.models.creditor import Creditor, q_state
from app.models.debitor import Debitor, DebitorBase
from app.models.Users import User

TDB = TypeVar("TDB", bound=AllDbModels)
TPU = TypeVar("TPU", bound=AllModelsPublic)
TL = TypeVar("TL", bound=AllModelsList)


def create_object(
    obj_in: AllModelsCreate,
    Obj_type: Type[TDB],  # obj_type represents a class, so use Type
    additional_data: Optional[dict] = {},
) -> TDB:  # The return type is an instance of one of the models in AllDbModels
    """
    Create a new record in the database.

    Args:
        obj_in (AllModelsCreate): The data to create the record with.

        Obj_type (Type[AllDbModels]): The class type of the record to create (e.g., Address, Invoice, etc.).
        additional_data (dict): OPTIONAL Additional data to create the record with. passed to update  in model_validate

    Returns:
        AllDbModels: The created record instance.
    """
    with Session(engine) as session:
        # Create an instance of the object
        print(obj_in)
        obj = Obj_type().model_validate(obj_in, update=additional_data)
        print(obj)
        # Add the object to the session and commit the changes
        session.add(obj)
        session.commit()

        # Refresh the object to get the updated state
        session.refresh(obj)
        obj = cast(TDB, obj)

    return obj


def read_objects(
    Obj_type: Type[AllDbModels],
    page: int,
    limit: int,
    filter: AllModelsFilter,
    order_by: Any,
    returnType: Type[TL],
) -> TL:
    """
    Read all records from the database.

    Args:
        Obj_type (Type[AllDbModels]): The class type of the record to read (e.g., Address, Invoice, etc.).
        page (int): The page number for pagination.
        limit (int): The number of records per page.
        filter (Filter): The filter object to apply to the query.
        order_by (Any): The field(s) to order the results by."""

    with Session(engine) as session:
        # Apply the filter and order the results
        stmt = filter.filter(select(Obj_type).offset((page) * limit).limit(limit))
        print(stmt)
        # Execute the query
        out = session.exec(stmt).all()

        # Get the total number of records
        size = session.exec(
            select(func.count()).select_from(filter.filter(select(Obj_type)))
        ).one()

        # Create the response object
        items: AllModelsList = returnType(items=out, count=len(out), total=size)

    return items


def read_object(
    Obj_type: Type[TDB],  # obj_type represents a class, so use Type
    id: uuid.UUID,
    Return_type: Type[TPU],
) -> TPU:  # The return type is an instance of one of the models in AllDbModels
    """
    Read a record from the database.

    Args:
        Obj_type (Type[AllDbModels]): The class type of the record to read (e.g., Address, Invoice, etc.).
        id (uuid.UUID): The ID of the record to read.

    Returns:
        AllDbModels: The record instance.
    """
    with Session(engine) as session:
        # Fetch the item by ID
        obj: TDB | None = session.get(Obj_type, id)

        # If the item is not found, raise a 404 HTTP exception
        if not obj:
            raise HTTPException(status_code=404, detail="Item not found")
        # Ensure the item is an instance of one of the types in AllDbModels
        if not isinstance(obj, Obj_type):
            raise HTTPException(status_code=400, detail="Item type mismatch")
    return_obj: TPU = Return_type.model_validate(obj)

    return return_obj


def updated_object(
    update_keys: dict[str, uuid.UUID],
    update_data: SQLModel,  # Assuming AllModelsPublic is a subtype of SQLModel
    Obj_type: Type[TDB],
) -> TDB:
    """
    Update a record in the database, handling changes to key fields.

    Args:
        update_keys (Dict[str, Any]): A dictionary of key fields to identify the record to update.
        update_data (SQLModel): The data to update the record with.
        Obj_type (Type[TDB]): The class type of the record to update (e.g., Invoice, ItemstoInvoice).

    Returns:
        TDB: The updated record instance of the specific type.
    """
    with Session(engine) as session:
        # Build filter conditions based on the key fields
        conditions = [
            getattr(Obj_type, key) == value for key, value in update_keys.items()
        ]
        stmt = select(Obj_type).where(*conditions)

        item: TDB | None = session.exec(stmt).first()
        # If the item is not found, raise a 404 HTTP exception
        if not item:
            raise HTTPException(status_code=404, detail="Item not found")

        # Ensure the item is an instance of the specific type passed as Obj_type
        if not isinstance(item, Obj_type):
            raise HTTPException(status_code=400, detail="Item type mismatch")

        if hasattr(item, "time_modified"):
            item.time_modified = datetime.now()
            # item.sqlmodel_update({"time_modified":datetime.now()})

        # Update the item's fields
        item.sqlmodel_update(update_data)

        # Add the updated item to the session and commit the changes
        session.add(item)
        session.commit()

        # Refresh the item to get the updated state
        session.refresh(item)
        print(item)
        return item


def update_creditor_name(
    creditor_id: uuid.UUID,
) -> Creditor:
    """
    Update the name of a creditor in the database.

    Args:
        creditor_id (uuid.UUID): The ID of the creditor to update.

    Returns:
        Creditor: The updated creditor instance.
    """
    with Session(engine) as session:
        # Fetch the creditor by ID
        creditor: Creditor | None = session.get(Creditor, creditor_id)

        # If the creditor is not found, raise a 404 HTTP exception
        if not creditor:
            raise HTTPException(status_code=404, detail="Creditor not found")

        # Update the creditor's name
        creditor.namefunction()

        # Add the updated creditor to the session and commit the changes
        session.add(creditor)
        session.commit()

        # Refresh the creditor to get the updated state
        session.refresh(creditor)

    return creditor


def delete_object(
    Obj_type: Type[TDB],  # obj_type represents a class, so use Type
    id: uuid.UUID,
) -> TDB:  # The return type is an instance of one of the models in AllDbModels
    """
    Delete a record from the database.

    Args:
        Obj_type (Type[AllDbModels]): The class type of the record to delete (e.g., Address, Invoice, etc.).
        id (uuid.UUID): The ID of the record to delete.

    Returns:
        AllDbModels: The deleted record instance.
    """
    with Session(engine) as session:
        # Fetch the item by ID
        item: TDB | None = session.get(Obj_type, uuid.UUID(bytes=id.bytes))

        # If the item is not found, raise a 404 HTTP exception
        if not item:
            raise HTTPException(status_code=404, detail="Item not found")
        # Ensure the item is an instance of one of the types in AllDbModels
        if not isinstance(item, Obj_type):
            raise HTTPException(status_code=400, detail="Item type mismatch")

        # Delete the item from the session and commit the changes
        session.delete(item)
        session.commit()

    return item


def q_fields_creditor(
    current_user: User,
    creditor_in: Creditor,
    creditor: Creditor | None = None,
) -> None:
    """
    Check if a field has been updated by the user and update the corresponding field in the creditor object.

    Args:
        current_user (Any): The current user.
        field (str): The field to check.
        creditor_in (AllModelsCreate): The input creditor object.
        creditor (AllModelsCreate): The creditor object to update.
    """
    q_field(current_user, "qcomment", creditor_in, creditor)
    q_field(current_user, "q_check", creditor_in, creditor, default=q_state.open)
    q_field(current_user, "q_check_timestamp", creditor_in, creditor)
    q_field(current_user, "creator_id", creditor_in, creditor, default=current_user.id)
    q_field(
        current_user,
        "name",
        creditor_in,
        creditor,
        default=creditor.name if creditor else None,
    )
    q_field(
        current_user,
        "accounting_year",
        creditor_in,
        default=datetime.now().year,
    )


def q_fields_debitor(
    current_user: User,
    debitor_in: Debitor | DebitorBase,
    debitor: Debitor | None = None,
) -> None:
    """
    Check if a field has been updated by the user and update the corresponding field in the debitor object.

    Args:
        current_user (Any): The current user.
        field (str): The field to check.
        debitor_in (AllModelsCreate): The input debitor object.
        debitor (AllModelsCreate): The debitor object to update.
    """
    q_field(current_user, "qcomment", debitor_in, debitor)
    q_field(current_user, "q_check", debitor_in, debitor, default=q_state.open)
    q_field(current_user, "q_check_timestamp", debitor_in, debitor)
    q_field(current_user, "creator_id", debitor_in, debitor, default=current_user.id)
    q_field(
        current_user,
        "name",
        debitor_in,
        debitor,
        default=debitor.name if debitor else None,
    )
    q_field(
        current_user,
        "accounting_year",
        debitor_in,
        default=datetime.now().year,
    )
    q_field(
        current_user,
        "amount",
        debitor_in,
    )
