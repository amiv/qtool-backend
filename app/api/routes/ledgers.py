import uuid
from typing import Annotated, Any

from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from sqlmodel import Session, create_engine, func, insert, select

from app.api.auth import get_user_info, is_quaestor
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    read_object,
    read_objects,
    updated_object,
)
from app.core.config import engine
from app.model import Ledger, LedgerBase, LedgerFilter, LedgerPublic, LedgersPublic
from app.models.Users import User

router = APIRouter()


@router.get("/", response_model=LedgersPublic)
def read_ledgers(
    current_user: Annotated[User, Depends(get_user_info)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: LedgerFilter().create_flattened_model() = Depends(),
) -> LedgersPublic:
    """
    retrieve all ledgers, sorted by accountnumber.
    paginate the results.
    """
    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    return read_objects(
        Ledger, page, limit, filter, Ledger.accountnumber, LedgersPublic
    )


@router.get("/{id}", response_model=Ledger)
def read_ledger(
    current_user: Annotated[User, Depends(get_user_info)], id: uuid.UUID
) -> Ledger:
    """
    retrieve a single Ledger by id.
    """
    return read_object(Ledger, id, Ledger)


@router.post("/", response_model=Ledger)
def create_ledger(
    current_user: Annotated[User, Depends(is_quaestor)], ledger_in: LedgerBase
) -> Ledger:
    """
    create a new Ledger.
    """
    return create_object(ledger_in, Ledger)


@router.patch("/{id}", response_model=Ledger)
def update_ledger(
    current_user: Annotated[User, Depends(is_quaestor)],
    id: uuid.UUID,
    ledger_in: LedgerPublic,
) -> Ledger:
    """
    update an Ledger.
    """
    return updated_object({"id": id}, ledger_in, Ledger)


@router.delete("/{id}", response_model=Ledger)
def delete_ledger(
    current_user: Annotated[User, Depends(is_quaestor)], id: uuid.UUID
) -> Ledger:
    """
    delete an Ledger.
    """
    return delete_object(Ledger, id)
