import uuid
from typing import Annotated, Any

from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from sqlmodel import Session, create_engine, func, insert, select

from app.api.auth import get_user_info, is_quaestor
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    read_object,
    read_objects,
    updated_object,
)
from app.core.config import engine
from app.model import Kst, KstBase, KstFilter, KstPublic, KstsPublic
from app.models.Users import User

router = APIRouter()


@router.get("/", response_model=KstsPublic)
def read_ksts(
    current_user: Annotated[User, Depends(get_user_info)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: KstFilter().create_flattened_model() = Depends(),
) -> KstsPublic:
    """
    retrieve all ksts, sorted by accountnumber.
    paginate the results.
    """
    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    return read_objects(Kst, page, limit, filter, Kst.kst_number, KstsPublic)


@router.get("/kst-responsible", response_model=KstsPublic)
def read_ksts_responsible(
    current_user: Annotated[User, Depends(get_user_info)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: KstFilter().create_flattened_model() = Depends(),
) -> KstsPublic:
    """
    retrieve all ksts, where the user is responsible, sorted by accountnumber.
    paginate the results.
    """
    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    ksts = read_objects(Kst, page, 10000, filter, Kst.kst_number, KstsPublic)
    new_ksts = []
    for kst in ksts.items:
        if kst.owner in current_user.groups:
            new_ksts.append(kst)
    ksts.items = new_ksts[page * limit : (page + 1) * limit]
    ksts.total = len(new_ksts)
    return ksts


@router.get("/{id}", response_model=Kst)
def read_kst(
    current_user: Annotated[User, Depends(get_user_info)], id: uuid.UUID
) -> Kst:
    """
    retrieve a single Kst by id.
    """
    return read_object(Kst, id, Kst)


@router.post("/", response_model=Kst)
def create_kst(
    current_user: Annotated[User, Depends(is_quaestor)], kst_in: KstBase
) -> Kst:
    """
    create a new Kst.
    """
    return create_object(kst_in, Kst)


@router.patch("/{id}", response_model=Kst)
def update_kst(
    current_user: Annotated[User, Depends(is_quaestor)],
    id: uuid.UUID,
    kst_in: KstPublic,
) -> Kst:
    """
    update an Kst.
    """
    return updated_object({"id": id}, kst_in, Kst)


@router.delete("/{id}", response_model=Kst)
def delete_kst(
    current_user: Annotated[User, Depends(is_quaestor)], id: uuid.UUID
) -> Kst:
    """
    delete an Kst.
    """
    return delete_object(Kst, id)
