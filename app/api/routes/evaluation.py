import uuid
from typing import Annotated, Any, Dict, Optional, Type, Union, get_args, get_origin

import uuid6
from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from pydantic import BaseModel, ConfigDict
from sqlalchemy import String, asc, desc, text
from sqlmodel import (
    Field,
    Relationship,
    Session,
    SQLModel,
    cast,
    create_engine,
    func,
    insert,
    select,
)

from app.api.auth import get_user_info
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    read_object,
    read_objects,
    updated_object,
)
from app.core.config import engine
from app.models.creditor import Creditor
from app.models.debitor import Debitor
from app.models.ksts import Kst, KstFilter, KstPublic, KstsPublic
from app.models.Users import User

router = APIRouter()


class KstEval(KstPublic):
    effective_plus: int
    effective_minus: int


class KstsEval(SQLModel):
    items: list[KstEval]
    count: int
    total: int


@router.get("/", response_model=KstsEval)
def read_Ksts(
    current_user: Annotated[User, Depends(get_user_info)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
) -> KstsEval:
    """
    Retrieve all the ksts and then add the effective_plus and effective_minus fields.
    calculated from all the creditors/debitors.
    """
    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    ksts = read_objects(Kst, page, limit, KstFilter(), Kst.id, KstsPublic)
    with Session(engine) as session:
        query = select(
            Creditor.kst_id, func.sum(Creditor.amount).label("total")
        ).group_by(Creditor.kst_id)
        creditors_list = session.exec(query).all()
        creditors = {creditor.kst_id: creditor.total for creditor in creditors_list}
        query = select(
            Debitor.kst_id, func.sum(Debitor.amount).label("total")
        ).group_by(Debitor.kst_id)
        debitors_list = session.exec(query).all()
        debitors = {debitor.kst_id: debitor.total for debitor in debitors_list}
        print(debitors)
        ksts_eval = KstsEval(items=[], count=ksts.count, total=ksts.total)
        for kst in ksts.items:
            kst_eval = KstEval.model_validate(
                kst,
                update={
                    "effective_plus": debitors.get(kst.id, 0),
                    "effective_minus": creditors.get(kst.id, 0),
                },
            )
            kst_eval.effective_plus = debitors.get(kst.id, 0)
            kst_eval.effective_minus = creditors.get(kst.id, 0)
            ksts_eval.items.append(kst_eval)
    return ksts_eval
