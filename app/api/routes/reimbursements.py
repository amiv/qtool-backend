import json
import uuid
from datetime import datetime
from typing import Annotated, Any

from fastapi import (
    APIRouter,
    Depends,
    File,
    Form,
    HTTPException,
    Query,
    Request,
    UploadFile,
    status,
)
from fastapi_filter import FilterDepends
from sqlmodel import Session, create_engine, func, insert, select

from app.api.auth import get_user_info, is_auditor, is_quaestor, oauth2_scheme
from app.api.processors.email.prepare_email import (
    send_reimbursement_accepted_email,
    send_reimbursement_declined_email,
)
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    q_fields_creditor,
    read_object,
    read_objects,
    update_creditor_name,
    updated_object,
)
from app.api.routes.files import get_file
from app.core.auth_helper import lock_field, q_field
from app.core.config import engine
from app.model import (
    Address,
    Creditor,
    Debitor,
    Reimbursement,
    ReimbursementBase,
    ReimbursementCreate,
    ReimbursementFilter,
    ReimbursementPublic,
    ReimbursementsList,
)
from app.models.creditor import q_state
from app.models.Users import User

router = APIRouter()


@router.get("/", response_model=ReimbursementsList)
def read_Reimbursements(
    current_user: Annotated[User, Depends(is_auditor)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: ReimbursementFilter().create_flattened_model() = Depends(),
) -> ReimbursementsList:
    """
    retrieve all Reimbursementes, sorted by name.
    paginate the results.
    """

    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    objects = read_objects(
        Reimbursement,
        page,
        limit,
        filter,
        Reimbursement.id,
        ReimbursementsList,
    )
    for object in objects.items:
        object.receipt = get_file(current_user, object.receipt)
    return objects


@router.get("/{id}", response_model=ReimbursementPublic)
def read_Reimbursement(
    current_user: Annotated[User, Depends(get_user_info)], id: uuid.UUID
) -> ReimbursementPublic:
    """
    retrieve a single Reimbursement by id.
    """

    object: ReimbursementPublic = read_object(Reimbursement, id, ReimbursementPublic)
    base_object: Reimbursement = read_object(Reimbursement, id, Reimbursement)
    if not (current_user.role == "quaestor" or current_user.role == "auditor"):
        if object.creditor.creator_id != current_user.id:
            if not (base_object.creditor.kst.owner in current_user.groups):
                raise HTTPException(status_code=401, detail="Only admins can see this")
    object.receipt = get_file(current_user, object.receipt)
    return object


@router.post("/", response_model=ReimbursementPublic)
def create_Reimbursement(
    current_user: Annotated[User, Depends(get_user_info)],
    reimbursement_in: ReimbursementCreate,
) -> ReimbursementPublic:
    """
    create a new Reimbursement.
    """
    q_fields_creditor(current_user, reimbursement_in.creditor)
    q_field(current_user, "ezag_timestamp", reimbursement_in)
    q_field(current_user, "ezag_id", reimbursement_in)

    creditor = Creditor().model_validate(reimbursement_in.creditor)
    with Session(engine) as session:
        session.add(creditor)
        session.commit()
        session.refresh(creditor)
        reimbursement = Reimbursement().model_validate(
            reimbursement_in,
            update={
                "creditor": creditor,
                "creditor_id": creditor.id,
                "name": "testname",
            },
        )
        session.add(reimbursement)
        reimbursement.creditor.namefunction()
        session.commit()
        session.refresh(reimbursement)
    print(reimbursement)
    returnReimbursement = ReimbursementPublic.model_validate(reimbursement)
    returnReimbursement.receipt = get_file(current_user, returnReimbursement.receipt)
    return returnReimbursement


@router.patch("/{id}", response_model=ReimbursementPublic)
def update_Reimbursement(
    current_user: Annotated[User, Depends(get_user_info)],
    id: uuid.UUID,
    reimbursement_in: ReimbursementPublic,
) -> ReimbursementPublic:
    """
    update an Reimbursement.
    """
    initial_object: Reimbursement = read_object(Reimbursement, id, Reimbursement)
    if initial_object.creditor.q_check == q_state.accepted:
        # only quaestor can change anything
        if current_user.role != "quaestor":
            raise HTTPException(status_code=403, detail="Only quaestor can change this")
    q_fields_creditor(current_user, reimbursement_in.creditor, initial_object.creditor)
    q_field(current_user, "ezag_timestamp", reimbursement_in, initial_object)
    q_field(current_user, "ezag_id", reimbursement_in, initial_object)
    if initial_object.ezag_id:
        lock_field("recipient", reimbursement_in, initial_object)
        lock_field("amount", reimbursement_in.creditor, initial_object.creditor)

    if (
        reimbursement_in.creditor.q_check == q_state.accepted
        and initial_object.creditor.q_check == q_state.open
    ):
        # send accepted email
        send_reimbursement_accepted_email(
            initial_object.id,
            initial_object.creditor.creator.nethz,
            initial_object.creditor.comment,
            initial_object.creditor.amount,
        )
    elif (
        reimbursement_in.creditor.q_check == q_state.rejected
        and initial_object.creditor.q_check == q_state.open
    ):
        # send declined email
        send_reimbursement_declined_email(
            initial_object.id,
            initial_object.creditor.creator.nethz,
            initial_object.creditor.qcomment,
            initial_object.creditor.amount,
        )

    if (
        initial_object.creditor.q_check == q_state.rejected
        and current_user.role != "quaestor"
    ):
        # if the user has a rejected bill it is set to open again after any change.
        reimbursement_in.creditor.q_check = q_state.open
    updated_object(
        {"id": initial_object.creditor.id}, reimbursement_in.creditor, Creditor
    )
    reimbursement = Reimbursement().model_validate(
        reimbursement_in,
        update={
            "creditor": reimbursement_in.creditor,
            "creditor_id": initial_object.creditor.id,
        },
    )
    if not initial_object.creditor.q_check == q_state.accepted:
        print(reimbursement.creditor)
        # reimbursement.creditor.namefunction()
    returnReimbursement = ReimbursementPublic.model_validate(
        updated_object({"id": id}, reimbursement, Reimbursement)
    )
    update_creditor_name(reimbursement.creditor_id)
    returnReimbursement.creditor.name = reimbursement.creditor.name
    returnReimbursement.receipt = get_file(current_user, returnReimbursement.receipt)

    return returnReimbursement


@router.delete("/{id}", response_model=Reimbursement)
def delete_Reimbursement(
    current_user: Annotated[User, Depends(is_quaestor)], id: uuid.UUID
) -> Reimbursement:
    """
    Delete a Reimbursement and its associated Creditor.
    """

    delete_message = delete_object(Reimbursement, id)
    # Delete the associated Creditor if it exists
    delete_object(Creditor, delete_message.creditor_id)

    # Delete the Reimbursement
    return delete_message
