import uuid
from typing import Annotated, Any

from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from sqlmodel import Session, create_engine, func, insert, select

from app.api.auth import get_user_info, is_quaestor
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    read_object,
    read_objects,
    updated_object,
)
from app.core.config import engine
from app.model import Item, ItemBase, ItemFilter, ItemPublic, ItemsPublic
from app.models.Users import User

router = APIRouter()


@router.get("/", response_model=ItemsPublic)
def read_items(
    current_user: Annotated[User, Depends(get_user_info)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: ItemFilter().create_flattened_model() = Depends(),
) -> ItemsPublic:
    """
    retrieve all items, sorted by id.
    paginate the results.
    """
    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    return read_objects(Item, page, limit, filter, Item.id, ItemsPublic)


@router.get("/{id}", response_model=Item)
def read_item(
    current_user: Annotated[User, Depends(get_user_info)], id: uuid.UUID
) -> Item:
    """
    retrieve a single item by id.
    """
    return read_object(Item, id, Item)


@router.post("/", response_model=Item)
def create_item(
    current_user: Annotated[User, Depends(is_quaestor)], item_in: ItemBase
) -> Item:
    """
    create a new item.
    """
    return create_object(item_in, Item)


@router.patch("/{id}", response_model=Item)
def update_item(
    current_user: Annotated[User, Depends(is_quaestor)],
    id: uuid.UUID,
    item_in: ItemPublic,
) -> Item:
    """
    update an item.
    """
    return updated_object({"id": id}, item_in, Item)


@router.delete("/{id}", response_model=Item)
def delete_item(
    current_user: Annotated[User, Depends(is_quaestor)], id: uuid.UUID
) -> Item:
    """
    delete an item.
    """
    return delete_object(Item, id)
