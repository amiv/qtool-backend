import json
import uuid
from typing import Annotated, Any

from fastapi import (
    APIRouter,
    Depends,
    File,
    Form,
    HTTPException,
    Query,
    Request,
    UploadFile,
)
from fastapi_filter import FilterDepends
from sqlmodel import Session, create_engine, func, insert, select

from app.api.auth import (
    get_api_user,
    get_user_info,
    is_auditor,
    is_onboarded,
    is_quaestor,
)
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    read_object,
    read_objects,
    updated_object,
)
from app.core.config import engine
from app.model import Address
from app.models.addresses import UserAddress
from app.models.Users import (
    DbUser,
    DbUserBase,
    DbUserCreate,
    DbUserFilter,
    DbUserPublic,
    DbUsersList,
    User,
)

router = APIRouter()

"""
TO handle users:
get user info by requesting session info from the api using a session token.
this returnts all the relevant info
we need to store the iban and address everything else can be requested from the api.

"""


@router.get("/", response_model=DbUsersList)
def read_Users(
    current_user: Annotated[User, Depends(is_auditor)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
) -> DbUsersList:
    """
    Retrieve all Users, sorted by id.
    Paginate the results.
    """
    return read_objects(DbUser, page, limit, DbUserFilter(), DbUser.id, DbUsersList)


@router.get("/{user_id}", response_model=DbUserPublic)
def read_User(
    user_id: str,
    current_user: Annotated[User, Depends(is_auditor)],
) -> DbUserPublic:
    """
    Retrieve a single User by id.
    """
    return read_object(DbUser, user_id, DbUserPublic)


@router.post("/", response_model=DbUserBase)
def create_User(
    *,
    user_in: DbUserCreate,
    current_user: Annotated[Any, Depends(get_api_user)],
) -> DbUserBase:
    """
    Create a new User.
    creditor = Creditor().model_validate(creditPayment_in.creditor)
    with Session(engine) as session:
        session.add(creditor)
        session.commit()
        session.refresh(creditor)
        creditPayment = CreditPayment().model_validate(
            creditPayment_in,
            update={
                "creditor": creditor,
                "creditor_id": creditor.id,
            },
        )
        session.add(creditPayment)
        session.commit()
        session.refresh(creditPayment)
    print(creditPayment)
    returncreditPayment = CreditPaymentPublic.model_validate(creditPayment)
    return returncreditPayment
    """
    if user_in.id != current_user["_id"]:
        raise HTTPException(
            status_code=401, detail="You can only create a user for yourself"
        )

    adress = UserAddress().model_validate(user_in.address)
    with Session(engine) as session:
        session.add(adress)
        session.commit()
        session.refresh(adress)
        # print(user_in)
        user = DbUser().model_validate(
            user_in,
            update={
                "address": adress,
                "address_id": adress.id,
                "nethz": current_user["nethz"],
            },
        )
        session.add(user)
        session.commit()
        session.refresh(user)
    returnuser = DbUserPublic.model_validate(user)
    return returnuser


@router.put("/{user_id}", response_model=DbUserBase)
def update_User(
    user_id: str,
    User_in: DbUserPublic,
    current_user: Annotated[User, Depends(get_user_info)],
) -> DbUserBase:
    """
    Update a User.
    """
    if User_in.id != current_user.id or user_id != current_user.id:
        raise HTTPException(
            status_code=401, detail="You can only update a user for yourself"
        )
    # CHECK FOR CHANGED ID
    print(User_in)
    updated_object({"id": User_in.address.id}, User_in.address, UserAddress)
    new_user = DbUser().model_validate(
        User_in,
        update={
            "address": User_in.address,
        },
    )
    return DbUserPublic.model_validate(
        updated_object({"id": user_id}, new_user, DbUser)
    )


@router.delete("/{user_id}", response_model=DbUserBase)
def delete_User(
    user_id: str,
    current_user: Annotated[User, Depends(is_quaestor)],
) -> DbUserBase:
    """
    Delete a User.
    """
    return delete_object(DbUser, user_id)
