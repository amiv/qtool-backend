import uuid
from datetime import datetime
from typing import Annotated, Any, Dict, Optional, Type, Union, get_args, get_origin

from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from sqlmodel import Session, SQLModel, create_engine, func, insert, select

from app.api.auth import get_user_info, is_auditor, is_quaestor
from app.api.processors.email.prepare_email import (
    send_credit_card_accepted_email,
    send_credit_card_declined_email,
)
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    q_fields_creditor,
    read_object,
    read_objects,
    update_creditor_name,
    updated_object,
)
from app.api.routes.files import get_file
from app.core.auth_helper import q_field
from app.core.config import engine
from app.model import (
    Address,
    Creditor,
    CreditPayment,
    CreditPaymentBase,
    CreditPaymentCreate,
    CreditPaymentFilter,
    CreditPaymentPublic,
    CreditPaymentsList,
    Debitor,
)
from app.models.creditor import q_state
from app.models.Users import User

router = APIRouter()


@router.get("/", response_model=CreditPaymentsList)
def read_creditPayments(
    current_user: Annotated[User, Depends(is_auditor)],
    filters: CreditPaymentFilter().create_flattened_model() = Depends(),
    page: int = 0,
    limit: int = Query(default=100, le=1000),
) -> CreditPaymentsList:
    """
    retrieve all CreditPaymentes, sorted by name.
    paginate the results.
    """
    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    return read_objects(
        CreditPayment,
        page,
        limit,
        filters,
        CreditPayment.id,
        CreditPaymentsList,
    )


@router.get("/{id}", response_model=CreditPaymentPublic)
def read_creditPayment(
    id: uuid.UUID, current_user: Annotated[User, Depends(get_user_info)]
) -> CreditPaymentPublic:
    """
    retrieve a single CreditPayment by id.
    """
    object = read_object(CreditPayment, id, CreditPaymentPublic)
    base_object: CreditPayment = read_object(CreditPayment, id, CreditPayment)
    if not (current_user.role == "quaestor" or current_user.role == "auditor"):
        if object.creditor.creator_id != current_user.id:
            if not (base_object.creditor.kst.owner in current_user.groups):
                raise HTTPException(status_code=401, detail="Only admins can see this")
    object.receipt = get_file(current_user, object.receipt)
    return object


@router.post("/", response_model=CreditPaymentPublic)
def create_creditPayment(
    current_user: Annotated[User, Depends(get_user_info)],
    creditPayment_in: CreditPaymentCreate,
) -> CreditPaymentPublic:
    """
    create a new CreditPayment.
    """
    q_fields_creditor(current_user, creditPayment_in.creditor)
    q_field(current_user, "ezag_timestamp", creditPayment_in)
    creditor = Creditor().model_validate(creditPayment_in.creditor)
    with Session(engine) as session:
        session.add(creditor)
        # session.commit()
        # session.refresh(creditor)
        creditPayment = CreditPayment().model_validate(
            creditPayment_in,
            update={
                "creditor": creditor,
                "creditor_id": creditor.id,
                "name": "CreditPayment",
            },
        )
        session.add(creditPayment)
        session.commit()
        session.refresh(creditPayment)
        creditPayment.creditor.namefunction()
        session.commit()
        session.refresh(creditPayment)
    print(creditPayment)
    creditPayment.receipt = get_file(current_user, creditPayment.receipt)
    returncreditPayment = CreditPaymentPublic.model_validate(creditPayment)

    return returncreditPayment


@router.patch("/{id}", response_model=CreditPaymentPublic)
def update_creditPayment(
    current_user: Annotated[User, Depends(get_user_info)],
    id: uuid.UUID,
    creditPayment_in: CreditPaymentPublic,
) -> CreditPaymentPublic:
    """
    update an CreditPayment.
    """
    initial_object: CreditPayment = read_object(CreditPayment, id, CreditPayment)
    if initial_object.creditor.q_check == q_state.accepted:
        # only quaestor can change anything
        if current_user.role != "quaestor":
            raise HTTPException(status_code=404, detail="Only quaestor can change this")
    q_fields_creditor(current_user, creditPayment_in.creditor, initial_object.creditor)
    q_field(current_user, "ezag_timestamp", creditPayment_in, initial_object.creditor)
    if (
        creditPayment_in.creditor.q_check == q_state.accepted
        and initial_object.creditor.q_check == q_state.open
    ):
        # send accepted email
        send_credit_card_accepted_email(
            initial_object.id,
            initial_object.creditor.creator.nethz,
            initial_object.creditor.comment,
            initial_object.creditor.amount,
        )
    elif (
        creditPayment_in.creditor.q_check == q_state.rejected
        and initial_object.creditor.q_check == q_state.open
    ):
        # send declined email
        send_credit_card_declined_email(
            initial_object.id,
            initial_object.creditor.creator.nethz,
            initial_object.creditor.qcomment,
            initial_object.creditor.amount,
        )

    if (
        initial_object.creditor.q_check == q_state.rejected
        and current_user.role != "quaestor"
    ):
        # if the user has a rejected bill it is set to open again after any change.
        creditPayment_in.creditor.q_check = q_state.open
    updated_object(
        {"id": creditPayment_in.creditor.id}, creditPayment_in.creditor, Creditor
    )
    creditPayment = CreditPayment().model_validate(
        creditPayment_in,
        update={
            "creditor": creditPayment_in.creditor,
            "creditor_id": creditPayment_in.creditor.id,
        },
    )
    returncredit_payment = CreditPaymentPublic.model_validate(
        updated_object({"id": id}, creditPayment, CreditPayment)
    )
    update_creditor_name(creditPayment.creditor_id)
    returncredit_payment.creditor.name = creditPayment.creditor.name
    returncredit_payment.receipt = get_file(current_user, returncredit_payment.receipt)
    return returncredit_payment


@router.delete("/{id}", response_model=CreditPayment)
def delete_creditPayment(
    current_user: Annotated[User, Depends(is_quaestor)], id: uuid.UUID
) -> CreditPayment:
    """
    delete an CreditPayment.
    """
    delete_message = delete_object(CreditPayment, id)
    # Delete the associated Creditor if it exists
    delete_object(Creditor, delete_message.creditor_id)

    return delete_message
