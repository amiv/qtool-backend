import uuid
from typing import Annotated, Any, Dict, Optional, Type, Union, get_args, get_origin

import uuid6
from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from pydantic import BaseModel, ConfigDict
from sqlalchemy import String, asc, desc, text
from sqlmodel import (
    Field,
    Relationship,
    Session,
    SQLModel,
    cast,
    create_engine,
    func,
    insert,
    select,
)

from app.api.auth import (
    get_user_groups,
    get_user_info,
    is_auditor,
    is_kst_responsible,
    is_quaestor,
)
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    read_object,
    read_objects,
    updated_object,
)
from app.core.config import engine
from app.model import (
    Address,
    Creditor,
    CreditPayment,
    CreditPaymentBase,
    CreditPaymentCreate,
    CreditPaymentFilter,
    CreditPaymentPublic,
    CreditPaymentsList,
    Debitor,
    Reimbursement,
)
from app.models.addresses import AddressFilter
from app.models.bills import Bill, BillFilter
from app.models.creditor import CreditorFilter, CreditorPublic, q_state
from app.models.creditPayments import Card
from app.models.filter import BaseFilter
from app.models.internalTransfers import InternalTransfer
from app.models.ksts import Kst
from app.models.ledgers import Ledger
from app.models.reimbursements import ReimbursementFilter
from app.models.Users import DbUser, DbUserFilter, User

router = APIRouter()


class CombinedCreditor(SQLModel):
    creditor: CreditorPublic
    type: Optional[str]
    id: Optional[uuid.UUID]
    card: Optional[Card]
    address: Optional[Address]
    reference: Optional[str]
    iban: Optional[str]
    recipient: Optional[str]
    ezag: Optional[uuid.UUID]
    creator: Optional[str]


class CombinedCreditorIn(BaseFilter):
    creditor: Optional[CreditorFilter] = None
    own: Optional[bool] = None
    type: Optional[str] = None
    id: Optional[uuid.UUID] = None
    receipt: Optional[str] = None
    card: Optional[Card] = None
    address: Optional[Address] = None
    reference: Optional[str] = None
    iban: Optional[str] = None
    recipient: Optional[str] = None
    responsible: Optional[bool] = None
    ezag: Optional[str] = None
    creator: Optional[str] = None


class CombinedCreditorFilter(BaseFilter):
    type: Annotated[list[str] | None, Query()] = None
    Creditor: Optional[CreditorFilter] = None
    CreditPayment: Optional[CreditPaymentFilter] = None
    Bill: Optional[BillFilter] = None
    reimbursement: Optional[ReimbursementFilter] = None
    own: Optional[bool] = None
    responsible: Optional[bool] = None

    class Constants:
        model = CombinedCreditorIn
        search_field_name = "search"


class CombinedCreditorList(SQLModel):
    items: list[CombinedCreditor]
    count: int
    total: int


@router.get("/unchecked", response_model=CombinedCreditorList)
def read_unchecked_CombinedPayments(
    current_user: Annotated[User, Depends(is_quaestor)],
    filters: CombinedCreditorIn().create_flattened_model() = Depends(),
    page: int = 0,
    limit: int = Query(default=100, le=1000),
) -> CombinedCreditorList:
    filters.creditor__q_check = q_state.open.value
    return read_CombinedPayments(current_user, filters, page, limit)


@router.get("/responsible", response_model=CombinedCreditorList)
def read_responsible_CombinedPayments(
    current_user: Annotated[User, Depends(is_kst_responsible)],
    filters: CombinedCreditorIn().create_flattened_model() = Depends(),
    page: int = 0,
    limit: int = Query(default=100, le=1000),
) -> CombinedCreditorList:
    filters.responsible = True
    return read_CombinedPayments(current_user, filters, page, limit)


@router.get("/own", response_model=CombinedCreditorList)
def read_own_CombinedPayments(
    current_user: Annotated[User, Depends(get_user_info)],
    filters: CombinedCreditorIn().create_flattened_model() = Depends(),
    page: int = 0,
    limit: int = Query(default=100, le=1000),
) -> CombinedCreditorList:
    """
    retrieve all payments, sorted by name."""
    filters.creditor__creator = current_user.nethz
    return read_CombinedPayments(current_user, filters, page, limit)


@router.get("/", response_model=CombinedCreditorList)
def read_CombinedPayments(
    current_user: Annotated[User, Depends(is_auditor)],
    filters: CombinedCreditorIn().create_flattened_model() = Depends(),
    page: int = 0,
    limit: int = Query(default=100, le=1000),
) -> CombinedCreditorList:
    """
    retrieve all payments, sorted by name."""
    print(filters)
    print(filters.type if filters.type else None)
    selectorlist = [CreditPayment, Bill, Reimbursement, InternalTransfer, DbUser]

    query = (
        select(Creditor, *selectorlist)
        .join(Kst)
        .join(Ledger)
        .outerjoin(DbUser, Creditor.creator_id == DbUser.id)
        .outerjoin(CreditPayment, Creditor.id == CreditPayment.creditor_id)
        .outerjoin(Bill, Creditor.id == Bill.creditor_id)
        .outerjoin(Reimbursement, Creditor.id == Reimbursement.creditor_id)
        .outerjoin(InternalTransfer, Creditor.id == InternalTransfer.creditor_id)
    )
    if filters.sort:
        sort_item, sort_order = filters.sort.split(":")
        sort_item_list = sort_item.split("__")
        field = Creditor
        if sort_item_list[0] == "creditor":
            if sort_item_list[1] == "kst":
                field = getattr(Kst, sort_item_list[2])
            elif sort_item_list[1] == "ledger":
                field = getattr(Ledger, sort_item_list[2])
            else:
                field = getattr(Creditor, sort_item_list[1])
            if sort_order == "desc":
                query = query.order_by(desc(field))
            else:
                query = query.order_by(asc(field))
        # query = query.order_by()
    # manual filters
    if filters.type:
        if "CreditPayment" in filters.type:
            query = query.filter(CreditPayment.id != None)
        if "Bill" in filters.type:
            query = query.filter(Bill.id != None)
        if "Reimbursement" in filters.type:
            query = query.filter(Reimbursement.id != None)
        if "InternalTransfer" in filters.type:
            query = query.filter(InternalTransfer.id != None)
    if filters.responsible:
        query = query.filter(cast(Kst.owner, String).in_(current_user.groups))
    if filters.creditor__amount:
        query = query.filter(
            cast(Creditor.amount, String).contains(filters.creditor__amount)
        )
    if filters.creditor__accounting_year:
        query = query.filter(
            cast(Creditor.accounting_year, String).contains(
                filters.creditor__accounting_year
            )
        )
    if filters.creditor__currency:
        query = query.filter(
            cast(Creditor.currency, String).contains(filters.creditor__currency)
        )
    if filters.creditor__comment:
        query = query.filter(
            cast(Creditor.comment, String).contains(filters.creditor__comment)
        )
    if filters.creditor__qcomment:
        query = query.filter(
            cast(Creditor.qcomment, String).contains(filters.creditor__qcomment)
        )
    if filters.creditor__q_check:
        query = query.filter(
            cast(Creditor.q_check, String).contains(str(filters.creditor__q_check))
        )
    if filters.creditor__q_check_timestamp:
        query = query.filter(
            cast(Creditor.q_check_timestamp, String).contains(
                filters.creditor__q_check_timestamp
            )
        )
    if filters.creditor__creator:
        query = query.filter(
            cast(DbUser.nethz, String).contains(filters.creditor__creator)
        )
    if filters.creditor__name:
        query = query.filter(
            cast(Creditor.name, String).contains(filters.creditor__name)
        )
    if filters.creditor__kst__kst_number:
        query = query.filter(
            cast(Kst.kst_number, String).contains(filters.creditor__kst__kst_number)
        )
    if filters.creditor__kst__name_de:
        query = query.filter(
            cast(Kst.name_de, String).contains(filters.creditor__kst__name_de)
        )
    if filters.creditor__kst__name_en:
        query = query.filter(
            cast(Kst.name_en, String).contains(filters.creditor__kst__name_en)
        )
    if filters.creditor__ledger__name_de:
        query = query.filter(
            cast(Ledger.name_de, String).contains(filters.creditor__ledger__name_de)
        )
    if filters.creditor__ledger__name_en:
        query = query.filter(
            cast(Ledger.name_en, String).contains(filters.creditor__ledger__name_en)
        )
    if filters.receipt:
        query = (
            query.filter(cast(CreditPayment.receipt, String).contains(filters.receipt))
            .filter(cast(Reimbursement.receipt, String).contains(filters.receipt))
            .filter(cast(Bill.receipt, String).contains(filters.receipt))
        )
    if filters.card:
        query = query.filter(cast(CreditPayment.card, String).contains(filters.card))
    if filters.address:
        query = query.filter(cast(Bill.address, String).contains(filters.address))
    if filters.reference:
        query = query.filter(cast(Bill.reference, String).contains(filters.reference))
    if filters.iban:
        query = query.filter(cast(Bill.iban, String).contains(filters.iban))
    if filters.recipient:
        query = query.filter(
            cast(Reimbursement.recipient, String).contains(filters.recipient)
        )
    if filters.ezag:
        query = query.filter(cast(Reimbursement.ezag_id, String).contains(filters.ezag))
    limited_query = query.offset(page * limit).limit(limit)
    # print(query)
    with Session(engine) as session:
        items = session.exec(limited_query).all()
        total = session.exec(select(func.count()).select_from(query)).one()

    # print(items)
    combined_items = []
    for item in items:
        # print(item)
        combined_items.append(
            CombinedCreditor(
                creditor=item.Creditor,
                creator=item.DbUser.nethz if item.DbUser else "Unknown",
                id=(
                    item.CreditPayment.id
                    if item.CreditPayment
                    else (
                        item.Bill.id
                        if item.Bill
                        else (
                            item.Reimbursement.id
                            if item.Reimbursement
                            else (
                                item.InternalTransfer.id
                                if item.InternalTransfer
                                else None
                            )
                        )
                    )
                ),
                type=(
                    "CreditPayment"
                    if item.CreditPayment
                    else (
                        "Bill"
                        if item.Bill
                        else (
                            "Reimbursement"
                            if item.Reimbursement
                            else "InternalTransfer" if item.InternalTransfer else None
                        )
                    )
                ),
                receipt=item.CreditPayment.receipt if item.CreditPayment else None,
                card=item.CreditPayment.card if item.CreditPayment else None,
                address=item.Bill.address if item.Bill else None,
                reference=item.Bill.reference if item.Bill else None,
                iban=item.Bill.iban if item.Bill else None,
                ezag=item.Reimbursement.ezag_id if item.Reimbursement else None,
                recipient=(
                    item.Reimbursement.recipient
                    if item.Reimbursement
                    else (
                        f"{item.InternalTransfer.debitor.kst.kst_number} {item.InternalTransfer.debitor.kst.name_de}"
                        if item.InternalTransfer
                        else None
                    )
                ),
            )
        )
    # print(combined_items)
    # print(combined_items)
    return CombinedCreditorList(
        items=combined_items, count=len(combined_items), total=total
    )
