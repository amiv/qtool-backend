import uuid
from datetime import datetime
from typing import Annotated, Any

from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from sqlalchemy.orm import selectinload
from sqlmodel import Session, create_engine, func, insert, select, update

from app.api.auth import get_user_info, is_auditor, is_kst_responsible, is_quaestor
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    q_fields_debitor,
    read_object,
    read_objects,
    updated_object,
)
from app.api.routes.evaluation import read_Ksts
from app.api.routes.items import read_item
from app.api.routes.ksts import read_kst
from app.core.auth_helper import q_field
from app.core.config import engine
from app.model import (
    Debitor,
    DebitorBase,
    Invoice,
    InvoiceBase,
    InvoiceCreate,
    InvoiceFilter,
    InvoicePublic,
    InvoicesList,
    Item,
    ItemsPublic,
    ItemstoInvoice,
    ItemstoInvoiceCreate,
)
from app.models.addresses import InvoiceAddress
from app.models.creditor import q_state
from app.models.invoices import ItemstoInvoicePublic
from app.models.items import ItemPublic
from app.models.ksts import Kst, KstFilter, KstsPublic
from app.models.Users import User

router = APIRouter()


@router.get("/{id}", response_model=InvoicePublic)
def read_Invoice(
    current_user: Annotated[User, Depends(get_user_info)], id: uuid.UUID
) -> InvoicePublic:
    """
    Retrieve a single Invoice by id.
    """
    ret_object = read_object(Invoice, id, InvoicePublic)
    if not (current_user.role == "quaestor" or current_user.role == "auditor"):
        if not current_user.role == "quaestor":
            kst = read_kst(current_user, ret_object.debitor.kst_id)
            if kst.owner not in current_user.groups:
                raise HTTPException(
                    status_code=403, detail="Only group owners can create this"
                )
    return ret_object


@router.get("/", response_model=InvoicesList)
def read_Invoices(
    current_user: Annotated[User, Depends(is_auditor)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: InvoiceFilter().create_flattened_model() = Depends(),
) -> InvoicesList:
    """
    Retrieve all Invoices, sorted by id.
    Paginate the results.
    """
    return read_objects(Invoice, page, limit, filter, Invoice.id, InvoicesList)


@router.post("/", response_model=InvoicePublic)
def create_Invoice(
    current_user: Annotated[User, Depends(is_kst_responsible)],
    Invoice_in: InvoiceCreate,
) -> InvoicePublic:
    """
    Create a new Invoice.
    """
    if not current_user.role == "quaestor":
        kst = read_kst(current_user, Invoice_in.debitor.kst_id)
        if kst.owner not in current_user.groups:
            raise HTTPException(
                status_code=403, detail="Only group owners can create this"
            )
    q_fields_debitor(current_user, Invoice_in.debitor)
    # Create the base invoice data
    invoice_data = Invoice_in.model_dump()
    additional_data: dict[str, Any] = {}

    print(Invoice_in)

    # Open a session for database operations
    with Session(engine) as session:
        # Validate and add the items to the invoice
        debitor = Debitor().model_validate(Invoice_in.debitor)
        session.add(debitor)
        additional_data["time_create"] = datetime.now()
        additional_data["time_modified"] = datetime.now()
        additional_data["debitor_id"] = debitor.id

        items = Invoice_in.items
        Invoice_in.items = []

        invoice: Invoice = Invoice().model_validate(Invoice_in, update=additional_data)
        print(items)
        total = 0
        for item_prototype in items:
            invoice.items.append(
                ItemstoInvoice(
                    item_id=item_prototype.item_id,
                    count=item_prototype.count,
                    amount=int(
                        item_prototype.count
                        * read_item(current_user, item_prototype.item_id).price
                    ),
                    comment=item_prototype.comment,
                )
            )
            total += int(
                item_prototype.count
                * read_item(current_user, item_prototype.item_id).price
            )
        # TODO will this work?
        invoice.debitor.amount = total

        # invoice_data.update(additional_data)

        # Add the Invoice object to the session and commit
        session.add(invoice)
        invoice.debitor.namefunction()
        session.commit()

        # Refresh to get the updated state from the DB
        session.refresh(invoice)
        print(f"invoice: {invoice}")
        print(f"invoice.items: {invoice.items}")

        # Create the public Invoice object
        print(f"additional_data: {additional_data}")
        public_invoice = InvoicePublic.model_validate(invoice)
    print(f"public_invoice: {public_invoice}")
    return public_invoice


@router.post("/createpdfInvoice/{invoice_id}")
def create_pdf_invoice(*, invoice_id: uuid.UUID):
    """
    Create a PDF of the invoice.
    """
    return {"invoice_id": invoice_id}


@router.patch("/{Invoice_id}", response_model=InvoicePublic)
def update_Invoice(
    current_user: Annotated[User, Depends(is_kst_responsible)],
    id: uuid.UUID,
    invoice_in: InvoicePublic,
) -> InvoicePublic:
    """
    update an Invoice.
    """

    initial_object: Invoice = read_object(Invoice, id, Invoice)
    if not current_user.role == "quaestor":
        kst = read_kst(current_user, initial_object.debitor.kst_id)
        if kst.owner not in current_user.groups:
            raise HTTPException(
                status_code=403, detail="Only group owners can change this"
            )

    q_fields_debitor(current_user, invoice_in.debitor, initial_object.debitor)
    if initial_object.debitor.q_check == q_state.accepted:
        # only quaestor can change anything
        if current_user.role != "quaestor":
            raise HTTPException(status_code=403, detail="Only quaestor can change this")
        invoice_in.debitor.q_check_timestamp = datetime.now()

    # Update each ItemstoInvoice record
    with Session(engine) as session:
        invoice = session.exec(select(Invoice).where(Invoice.id == id)).one()
        if not invoice:
            raise HTTPException(status_code=404, detail="Invoice not found")
        # Update the Invoice object

        invoice.debitor.sqlmodel_update(invoice_in.debitor)
        invoice.address.sqlmodel_update(invoice_in.address)
        # update/delete all the existing entries
        for item in invoice.items:
            if item.item_id not in [item2.item.id for item2 in invoice_in.items]:
                session.delete(item)
            item_prototype = next(
                item2 for item2 in invoice_in.items if item2.item.id == item.item_id
            )
            item.sqlmodel_update(item_prototype)
        # create any new entries
        for item in invoice_in.items:
            if item.item.id not in [item2.item_id for item2 in invoice.items]:
                invoice.items.append(
                    ItemstoInvoice(
                        item_id=item.item.id,
                        count=item.count,
                        amount=item.amount,
                        comment=item.comment,
                    )
                )
        # update everything else:
        invoice_dict = invoice_in.model_dump()
        invoice_dict.pop("items")
        invoice_dict.pop("debitor")
        invoice_dict.pop("address")
        invoice.sqlmodel_update(invoice_dict)
        session.commit()
        session.refresh(invoice)

    return InvoicePublic.model_validate(invoice)


@router.delete("/{id}", response_model=Invoice)
def delete_Invoice(
    current_user: Annotated[User, Depends(is_quaestor)], id: uuid.UUID
) -> Invoice:
    """
    delete an Invoice.
    """
    return delete_object(Invoice, id)
