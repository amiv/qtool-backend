import io
import os
import uuid
from datetime import timedelta  # Import timedelta
from typing import Annotated, Optional

import uuid6
from fastapi import APIRouter, Depends, FastAPI, File, HTTPException, UploadFile, status
from fastapi.responses import RedirectResponse
from minio import Minio
from minio.error import S3Error
from sqlmodel import Session, select

from app.api.auth import get_user_info
from app.core.config import client, engine, settings
from app.models.unlinked_files import UnlinkedFiles
from app.models.Users import User

""" # Create a bucket if it doesn't exist
def create_bucket(client: Minio, bucket_name: str):
    try:
        found = client.bucket_exists(bucket_name)
        if not found:
            client.make_bucket(bucket_name)
            print(f"Bucket '{bucket_name}' created.")
        else:
            print(f"Bucket '{bucket_name}' already exists.")
    except S3Error as exc:
        print(f"Error checking/creating bucket: {exc}")
        raise


# Initialize MinIO and ensure bucket exists on startup
client = initialize_minio_client()
create_bucket(client, MINIO_BUCKET_NAME) """

router = APIRouter()


@router.get("/unlinked_files", status_code=200)
async def get_unlinked_files(
    current_user: Annotated[User, Depends(get_user_info)],
):
    """
    Get a list of all files in the MinIO bucket that are not linked to any item.
    """
    try:
        with Session(engine) as session:
            unlinked_files = session.exec(select(UnlinkedFiles)).all()
    except S3Error as exc:
        print(f"Error listing unlinked files: {exc}")
        raise HTTPException(status_code=500, detail="Failed to list unlinked files.")

    return {"unlinked_files": unlinked_files}


def upload_xml(name: uuid.UUID, file: str):
    file_uuid = str(name)
    object_name = file_uuid  # Using UUID as the object name

    try:
        # Read file content
        file_content = file
        # Create a BytesIO stream from the file content
        file_stream = io.BytesIO(bytes(file, "utf-8"))
        # Upload the file to MinIO
        client.put_object(
            bucket_name=settings.MINIO_BUCKET_NAME,
            object_name=object_name,
            data=file_stream,  # Use BytesIO stream
            length=len(file_content),
            content_type="xml",
        )
    except S3Error as exc:
        print(f"Error uploading file: {exc}")
        raise HTTPException(status_code=500, detail="Failed to upload file.")

    return {"file_id": file_uuid}


@router.post("/upload", status_code=201)
async def upload_file(
    current_user: Annotated[User, Depends(get_user_info)], file: UploadFile = File(...)
):
    """
    Upload a file to MinIO and return its UUID.
    """
    file_uuid = str(uuid6.uuid7())
    object_name = file_uuid  # Using UUID as the object name

    try:
        # Read file content
        file_content = await file.read()
        # check max filesize
        if len(file_content) > 2000000:
            raise HTTPException(status_code=413, detail="File too large")
        # Create a BytesIO stream from the file content
        file_stream = io.BytesIO(file_content)
        # Upload the file to MinIO
        client.put_object(
            bucket_name=settings.MINIO_BUCKET_NAME,
            object_name=object_name,
            data=file_stream,  # Use BytesIO stream
            length=len(file_content),
            content_type=file.content_type,
        )
        with Session(engine) as session:
            unlinked_file = UnlinkedFiles().model_validate(
                {"id": file_uuid, "name": file.filename, "creator": current_user.id}
            )
            session.add(unlinked_file)
            session.commit()
    except S3Error as exc:
        print(f"Error uploading file: {exc}")
        raise HTTPException(status_code=500, detail="Failed to upload file.")

    return {"file_id": file_uuid}


@router.get("/files/{file_id}")
def get_file_request(
    current_user: Annotated[User, Depends(get_user_info)],
    file_id: str,
    expires: Optional[int] = 3600,
):
    """
    Generate a presigned URL for downloading the file from MinIO."""
    return RedirectResponse(get_file(current_user, file_id, expires))


def get_file(
    current_user: Annotated[User, Depends(get_user_info)],
    file_id: str,
    expires: Optional[int] = 3600,
):
    """
    Generate a presigned URL for downloading the file from MinIO.
    """
    file_id = str(file_id).strip()
    if file_id is None or file_id == "":
        return ""
    try:  # Verify if the object exists
        client.stat_object(settings.MINIO_BUCKET_NAME, file_id)
    except S3Error as exc:
        return "file doesn't exist"
    try:
        # Verify if the object exists
        client.stat_object(settings.MINIO_BUCKET_NAME, file_id)
    except S3Error as exc:
        if exc.code == "NoSuchKey":
            raise HTTPException(status_code=404, detail="File not found.")
        else:
            print(f"Error accessing file: {exc}")
            raise HTTPException(status_code=500, detail="Failed to retrieve file.")

    try:
        # Convert expires to timedelta
        expires_timedelta = timedelta(seconds=expires)
        # Generate a presigned URL
        url = client.presigned_get_object(
            bucket_name=settings.MINIO_BUCKET_NAME,
            object_name=file_id,
            expires=expires_timedelta,
        )
    except S3Error as exc:
        print(f"Error generating presigned URL: {exc}")
        raise HTTPException(status_code=500, detail="Failed to generate download URL.")

    return url


@router.patch("/files/{file_id}", status_code=200)
async def replace_file(
    current_user: Annotated[User, Depends(get_user_info)],
    file_id: str,
    file: UploadFile = File(...),
):
    """
    Replace an existing file in MinIO with a new file using the same UUID.
    """
    try:
        # Verify if the object exists
        client.stat_object(settings.MINIO_BUCKET_NAME, file_id)
    except S3Error as exc:
        if exc.code == "NoSuchKey":
            raise HTTPException(status_code=404, detail="File not found.")
        else:
            print(f"Error accessing file: {exc}")
            raise HTTPException(
                status_code=500, detail="Failed to access existing file."
            )

    try:
        # Read new file content
        file_content = await file.read()
        # Create a BytesIO stream from the new file content
        file_stream = io.BytesIO(file_content)
        # Replace the existing file
        client.put_object(
            bucket_name=settings.MINIO_BUCKET_NAME,
            object_name=file_id,
            data=file_stream,  # Use BytesIO stream
            length=len(file_content),
            content_type=file.content_type,
        )
    except S3Error as exc:
        print(f"Error replacing file: {exc}")
        raise HTTPException(status_code=500, detail="Failed to replace file.")

    return {"message": "File replaced successfully."}


# Optional: Endpoint to list all files (for debugging purposes)
@router.get("/files")
def list_files(
    current_user: Annotated[User, Depends(get_user_info)],
):
    """
    List all files in the MinIO bucket.
    """
    try:
        objects = client.list_objects(settings.MINIO_BUCKET_NAME, recursive=True)
        file_list = [obj.object_name for obj in objects]
    except S3Error as exc:
        print(f"Error listing files: {exc}")
        raise HTTPException(status_code=500, detail="Failed to list files.")

    return {"files": file_list}


# Optional: Endpoint to delete a file
@router.delete("/files/{file_id}", status_code=204)
def delete_file(current_user: Annotated[User, Depends(get_user_info)], file_id: str):
    """
    Delete a file from MinIO.
    """
    try:
        client.remove_object(settings.MINIO_BUCKET_NAME, file_id)
    except S3Error as exc:
        if exc.code == "NoSuchKey":
            raise HTTPException(status_code=404, detail="File not found.")
        else:
            print(f"Error deleting file: {exc}")
            raise HTTPException(status_code=500, detail="Failed to delete file.")

    return
