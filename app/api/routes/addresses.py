import uuid
from typing import Annotated, Any

from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from sqlmodel import Session, create_engine, func, insert, select

from app.api.auth import get_user_info, is_auditor, is_quaestor
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    read_object,
    read_objects,
    updated_object,
)
from app.core.config import engine
from app.model import (
    Address,
    AddressBase,
    AddressesPublic,
    AddressFilter,
    AddressPublic,
)
from app.models.Users import User

router = APIRouter()


@router.get("/", response_model=AddressesPublic)
def read_addresses(
    current_user: Annotated[User, Depends(is_auditor)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: AddressFilter().create_flattened_model() = Depends(),
) -> AddressesPublic:
    """
    retrieve all addresses, sorted by name.
    paginate the results.
    """
    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    return read_objects(Address, page, limit, filter, Address.name, AddressesPublic)


@router.get("/{id}", response_model=Address)
def read_address(
    current_user: Annotated[User, Depends(is_auditor)], id: uuid.UUID
) -> Address:
    """
    retrieve a single Address by id.
    TODO: find better permission set for this
    """
    return read_object(Address, id)


@router.post("/", response_model=Address)
def create_address(
    current_user: Annotated[User, Depends(get_user_info)], address_in: AddressBase
) -> Address:
    """
    create a new Address.
    """
    return create_object(address_in, Address)


@router.patch("/{id}", response_model=Address)
def update_address(
    current_user: Annotated[User, Depends(get_user_info)],
    id: uuid.UUID,
    address_in: AddressPublic,
) -> Address:
    """
    update an Address.
    """
    return updated_object({"id": id}, address_in, Address)


@router.delete("/{id}", response_model=Address)
def delete_address(
    current_user: Annotated[User, Depends(is_quaestor)], id: uuid.UUID
) -> Address:
    """
    delete an Address.
    """
    return delete_object(Address, id)
