import uuid
from typing import Annotated, Any

from fastapi import APIRouter, Depends, HTTPException, Query, Request
from fastapi_filter import FilterDepends
from sqlmodel import Session, create_engine, func, insert, select

from app.api.auth import get_user_info, is_auditor, is_quaestor
from app.api.processors.email.prepare_email import (
    send_bill_payment_accepted_email,
    send_bill_payment_declined_email,
)
from app.api.routes.api_helper import (
    create_object,
    delete_object,
    q_fields_creditor,
    read_object,
    read_objects,
    update_creditor_name,
    updated_object,
)
from app.api.routes.files import get_file
from app.core.auth_helper import q_field
from app.core.config import engine
from app.model import (
    Address,
    Bill,
    BillBase,
    BillCreate,
    BillFilter,
    BillPublic,
    BillsList,
    Creditor,
    Debitor,
)
from app.models.addresses import BillAddress
from app.models.creditor import q_state
from app.models.reimbursements import Reimbursement
from app.models.Users import User

router = APIRouter()


@router.get("/", response_model=BillsList)
def read_Bills(
    current_user: Annotated[User, Depends(is_auditor)],
    page: int = 0,
    limit: int = Query(default=100, le=1000),
    filter: BillFilter().create_flattened_model() = Depends(),
) -> BillsList:
    """
    retrieve all Bills, sorted by name.
    paginate the results.
    """
    # we need to have a smart way of ordering. THe fastapi_filter library does not support ordering.
    return read_objects(
        Bill,
        page,
        limit,
        filter,
        Bill.id,
        BillsList,
    )


@router.get("/{id}", response_model=BillPublic)
def read_Bill(
    current_user: Annotated[User, Depends(get_user_info)], id: uuid.UUID
) -> BillPublic:
    """
    retrieve a single Bill by id.
    """
    object = read_object(Bill, id, BillPublic)
    base_object: Bill = read_object(Bill, id, Bill)
    if not (current_user.role == "quaestor" or current_user.role == "auditor"):
        if object.creditor.creator_id != current_user.id:
            if not (base_object.creditor.kst.owner in current_user.groups):
                raise HTTPException(status_code=401, detail="Only admins can see this")
    object.receipt = get_file(current_user, object.receipt)
    return object


@router.post("/", response_model=BillPublic)
def create_Bill(
    current_user: Annotated[User, Depends(get_user_info)], bill_in: BillCreate
) -> BillPublic:
    """
    create a new Bill.
    """
    q_fields_creditor(current_user, bill_in.creditor)
    creditor = Creditor().model_validate(bill_in.creditor)
    address = BillAddress().model_validate(bill_in.address)
    with Session(engine) as session:
        session.add(creditor)
        session.add(address)
        # session.commit()
        # session.refresh(creditor)
        # session.refresh(address)
        bill = Bill().model_validate(
            bill_in,
            update={
                "creditor": creditor,
                "address": address,
                "creditor_id": creditor.id,
                "address_id": address.id,
            },
        )
        session.add(bill)
        session.commit()
        session.refresh(bill)
        bill.creditor.namefunction()
        session.commit()
        session.refresh(bill)
    print(bill)
    bill.receipt = get_file(current_user, bill.receipt)
    returnBill = BillPublic.model_validate(bill)
    return returnBill


@router.patch("/{id}", response_model=BillPublic)
def update_Bill(
    current_user: Annotated[User, Depends(get_user_info)],
    id: uuid.UUID,
    bill_in: BillPublic,
) -> BillPublic:
    """
    update an Bill.
    """
    initial_object: Bill = read_object(Bill, id, Bill)
    if initial_object.creditor.q_check == q_state.accepted:
        # only quaestor can change anything
        if current_user.role != "quaestor":
            raise HTTPException(status_code=403, detail="Only quaestor can change this")

    q_fields_creditor(current_user, bill_in.creditor, initial_object.creditor)
    q_field(current_user, "address_id", bill_in, initial_object.address_id)
    if (
        bill_in.creditor.q_check == q_state.accepted
        and initial_object.creditor.q_check == q_state.open
    ):
        # send accepted email
        send_bill_payment_accepted_email(
            initial_object.id,
            initial_object.creditor.creator.nethz,
            initial_object.creditor.comment,
            initial_object.creditor.amount,
        )
    elif (
        bill_in.creditor.q_check == q_state.rejected
        and initial_object.creditor.q_check == q_state.open
    ):
        # send declined email
        send_bill_payment_declined_email(
            initial_object.id,
            initial_object.creditor.creator.nethz,
            initial_object.creditor.qcomment,
            initial_object.creditor.amount,
        )

    if (
        initial_object.creditor.q_check == q_state.rejected
        and current_user.role != "quaestor"
    ):
        # if the user has a rejected bill it is set to open again after any change.
        bill_in.creditor.q_check = q_state.open
    updated_object({"id": bill_in.address.id}, bill_in.address, BillAddress)
    updated_object({"id": bill_in.creditor.id}, bill_in.creditor, Creditor)
    bill = Bill().model_validate(
        bill_in,
        update={
            "creditor": bill_in.creditor,
            "address": bill_in.address,
            "creditor_id": bill_in.creditor.id,
            "address_id": bill_in.address.id,
        },
    )
    returnBill = BillPublic.model_validate(updated_object({"id": id}, bill, Bill))
    update_creditor_name(bill.creditor_id)
    returnBill.creditor.name = bill.creditor.name

    returnBill.receipt = get_file(current_user, returnBill.receipt)
    return returnBill


@router.delete("/{id}", response_model=Bill)
def delete_Bill(
    current_user: Annotated[User, Depends(is_quaestor)], id: uuid.UUID
) -> Bill:
    """
    delete an Bill.
    """
    delete_message = delete_object(Bill, id)
    # Delete the associated Creditor if it exists
    delete_object(Creditor, delete_message.creditor_id)

    return delete_message
