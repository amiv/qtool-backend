import xml.etree.ElementTree as ET
from datetime import datetime
from xml.dom import minidom

from schwifty import BIC, IBAN

from app.models.reimbursements import Reimbursement

# Database setup (modify with your database specifics)


def create_iso20022_file(payments: list[Reimbursement], filename="output.xml"):
    # Create the root element with namespaces
    root = ET.Element(
        "Document",
        {
            "xmlns": "urn:iso:std:iso:20022:tech:xsd:pain.001.001.09",  # Correct namespace with colon
            "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",  # Correct namespace with colon
            "xsi:schemaLocation": "urn:iso:std:iso:20022:tech:xsd:pain.001.001.09 pain.001.001.09.ch.03.xsd",
        },  # Correct attribute with colon
    )
    # Create 'CstmrCdtTrfInitn' (Customer Credit Transfer Initiation)
    cstmr_cdt_trf_initn = ET.SubElement(root, "CstmrCdtTrfInitn")

    # Create 'GrpHdr' (Group Header)
    grp_hdr = ET.SubElement(cstmr_cdt_trf_initn, "GrpHdr")
    ET.SubElement(grp_hdr, "MsgId").text = "Msg-" + datetime.now().strftime("%Y%m%d")
    ET.SubElement(grp_hdr, "CreDtTm").text = datetime.now().isoformat()
    ET.SubElement(grp_hdr, "NbOfTxs").text = str(len(payments))
    ET.SubElement(grp_hdr, "CtrlSum").text = str(
        sum(float(item["creditor"]["amount"]) for item in payments) / 100.0
    )

    # Add Initiating Party (Payer) Information
    initg_pty = ET.SubElement(grp_hdr, "InitgPty")
    ET.SubElement(initg_pty, "Nm").text = "AMIV an der ETH"  # Example debtor name

    # Create 'PmtInf' (Payment Information) with fixed attributes for this example
    pmt_inf = ET.SubElement(cstmr_cdt_trf_initn, "PmtInf")
    ET.SubElement(pmt_inf, "PmtInfId").text = "PmtInf-" + datetime.now().strftime(
        "%Y%m%d"
    )
    ET.SubElement(pmt_inf, "PmtMtd").text = "TRF"  # Transfer
    ET.SubElement(pmt_inf, "BtchBookg").text = "false"

    # Create the ReqdExctnDt element
    reqd_exctn_dt = ET.SubElement(pmt_inf, "ReqdExctnDt")

    ET.SubElement(reqd_exctn_dt, "Dt").text = datetime.now().strftime("%Y-%m-%d")

    # Debtor (payer) details
    dbtr = ET.SubElement(pmt_inf, "Dbtr")
    ET.SubElement(dbtr, "Nm").text = "AMIV an der ETH"
    # pstl_adr = ET.SubElement(dbtr, "PstlAdr")
    # ET.SubElement(pstl_adr, "StrtNm").text = "Universitätstrasse 6"
    # ET.SubElement(pstl_adr, "PstCd").text = "8092"
    # ET.SubElement(pstl_adr, "TwnNm").text = "Zürich"
    # ET.SubElement(pstl_adr, "Ctry").text = "CH"

    dbtr_acct = ET.SubElement(pmt_inf, "DbtrAcct")
    dbtr_id = ET.SubElement(dbtr_acct, "Id")
    ET.SubElement(dbtr_id, "IBAN").text = "CH8009000000800032105"  # Example IBAN

    dbtr_agt = ET.SubElement(pmt_inf, "DbtrAgt")
    fin_instn_id = ET.SubElement(dbtr_agt, "FinInstnId")
    ET.SubElement(fin_instn_id, "BICFI").text = "POFICHBEXXX"  # Example BIC

    # For each payment record, create a 'CdtTrfTxInf' element
    for idx, payment in enumerate(payments):
        # Create Credit Transfer Transaction Information block
        cdt_trf_tx_inf = ET.SubElement(pmt_inf, "CdtTrfTxInf")

        # Payment Identification
        pmt_id = ET.SubElement(cdt_trf_tx_inf, "PmtId")
        ET.SubElement(pmt_id, "InstrId").text = (
            f"InstrId-{datetime.now().strftime('%y%m%d')}-{idx}"
        )
        ET.SubElement(pmt_id, "EndToEndId").text = payment.creditor.name
        amt = ET.SubElement(cdt_trf_tx_inf, "Amt")
        """
        # payment amount depending on currency and country of IBAN

        if payment["creditor"]["currency"] != "CHF" and payment["recipient"]["iban"][:2] in ["CH", "LI"]:
            # If the currency is not CHF and the IBAN starts with CH, use the EqvtAmt element to
            #  transfer in CHF and avoid fees
            eqvt_amt = ET.SubElement(amt, "EqvtAmt")
            amt2 = ET.SubElement(eqvt_amt, "Amt", Ccy=payment["creditor"]["currency"])
            amt2.text = str(payment["creditor"]["amount"])
            curroftran = ET.SubElement(eqvt_amt, "CcyOfTrf")
            curroftran.text = "CHF"
        else:
            # For all other payments, use the instdAmt element
            instd_amt = ET.SubElement(amt, "InstdAmt", Ccy=payment["creditor"]["currency"])
            instd_amt.text = str(payment["creditor"]["amount"])
            print(payment["recipient"]["iban"][:2] == "CH")
        """
        # amount transferred
        instd_amt = ET.SubElement(
            amt, "InstdAmt", Ccy=payment["creditor"]["currency"].value
        )
        instd_amt.text = str(payment["creditor"]["amount"] / 100.0)

        # Creditor Agent (Creditor's bank) details
        cdtr_agt = ET.SubElement(cdt_trf_tx_inf, "CdtrAgt")
        fin_instn_id = ET.SubElement(cdtr_agt, "FinInstnId")
        bic = BIC.from_bank_code(
            IBAN(payment["recipient"]["iban"]).country_code,
            IBAN(payment["recipient"]["iban"]).bank_code,
        )
        ET.SubElement(fin_instn_id, "BICFI").text = bic

        # Creditor Details
        cdtr = ET.SubElement(cdt_trf_tx_inf, "Cdtr")
        ET.SubElement(cdtr, "Nm").text = payment["recipient"]["address"]["name"]

        cdtr_acct = ET.SubElement(cdt_trf_tx_inf, "CdtrAcct")
        cdtr_id = ET.SubElement(cdtr_acct, "Id")
        ET.SubElement(cdtr_id, "IBAN").text = payment["recipient"]["iban"]

        # if qr bill (the if statement has to be correctly implemented later currently uses city when qcomment is yes):
        if False:
            rmt_inf = ET.SubElement(cdt_trf_tx_inf, "RmtInf")
            strd = ET.SubElement(rmt_inf, "Strd")
            cdtr_ref_inf = ET.SubElement(strd, "CdtrRefInf")
            tp = ET.SubElement(cdtr_ref_inf, "Tp")
            cd_or_prtry = ET.SubElement(tp, "CdOrPrtry")
            prtry = ET.SubElement(cd_or_prtry, "Prtry")
            prtry.text = "QRR"
            ref = ET.SubElement(cdtr_ref_inf, "Ref")
            ref.text = payment["recipient"]["city"]

    # Generate the XML tree and convert to string
    xml_str = ET.tostring(root, encoding="utf-8", xml_declaration=True).decode("utf-8")

    # Use minidom to prettify the XML string
    pretty_xml = minidom.parseString(xml_str).toprettyxml(indent="  ")

    finalxml = pretty_xml.replace(
        '<?xml version="1.0" ?>', '<?xml version="1.0" encoding="UTF-8"?>'
    )
    return finalxml
