from PyPDF2 import PdfReader, PdfWriter
from reportlab.lib.pagesizes import A4, letter
from reportlab.pdfgen import canvas


def add_footer_to_existing_pdf(input_pdf_path, output_pdf_path, text=""):
    # Create a temporary PDF to hold footers
    temp_pdf_path = "temp_with_footer.pdf"
    c = canvas.Canvas(temp_pdf_path, pagesize=letter)
    width, height = letter

    # Loop through each page and add the footer with three rows and two vertical lines
    for i in range(1, len(PdfReader(input_pdf_path).pages) + 1):
        # Set the font and size
        c.setFont("Helvetica", 9)

        # Draw footer text in three rows and three columns
        c.drawString(50, 60, "AMIV an der ETH")  # Column 1, Row 1
        c.drawString(50, 45, "MWST-Nr: CHE-218.868.772 MWST")  # Column 2, Row 1

        c.drawCentredString(300, 80, "Universitätstrasse 6")  # Column 1, Row 2
        c.drawCentredString(300, 65, "CAB E37")  # Column 2, Row 2
        c.drawCentredString(300, 50, "8092 Zürich")  # Column 3, Row 2
        c.drawCentredString(300, 35, "www.amiv.ethz.ch")  # Column 3, Row 2

        c.drawString(375, 90, "Bank: PostFinance AG")  # Column 1, Row 3
        c.drawString(375, 75, "Postkonto: 80-3210-5")  # Column 2, Row 3
        c.drawString(375, 60, "IBAN: CH80 0900 0000 8000 3210 5")  # Column 3, Row 3
        c.drawString(375, 45, "BLZ: 9000")
        c.drawString(375, 30, "BIC: POFICHBEXXX")
        # Draw vertical lines to separate columns
        c.line(250, 20, 250, 100)  # Line between Column 1 and 2
        c.line(350, 20, 350, 100)  # Line between Column 2 and 3

        # Add watermark
        c.setFont("Helvetica", 50)
        c.setFillGray(0.5, alpha=0.3)  # Set gray color and transparency
        c.saveState()
        c.translate(A4[0] / 2, A4[1] / 2)
        c.rotate(45)  # Rotate the text
        c.drawCentredString(0, 0, text)
        c.restoreState()

        # Move to the next page
        c.showPage()

    # Save the temporary footer PDF
    c.save()

    # Read the existing PDF and the temporary footer PDF
    reader = PdfReader(input_pdf_path)
    temp_reader = PdfReader(temp_pdf_path)
    writer = PdfWriter()

    # Merge each page of the original PDF with the corresponding footer
    for i in range(len(reader.pages)):
        original_page = reader.pages[i]
        footer_page = temp_reader.pages[i]
        original_page.merge_page(footer_page)
        writer.add_page(original_page)

    # Write the output PDF
    with open(output_pdf_path, "wb") as output_pdf:
        writer.write(output_pdf)
