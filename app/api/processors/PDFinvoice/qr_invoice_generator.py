import tempfile

from qrbill import QRBill
from reportlab.graphics import renderPDF
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas
from svglib.svglib import svg2rlg

# TO DO:
# - add invoice number to qr_invoice
# - add debitor data to qr_invoice
# - add account number to qr_invoice


# Calculate the checksum for the reference number
def modulo10(num_str):
    n_table = [0, 9, 4, 6, 8, 2, 7, 1, 3, 5]
    carry = 0

    for char in num_str:
        carry = n_table[(carry + int(char)) % 10]

    return (10 - carry) % 10


def calculate_ref_number(invoice_number):
    invoice_number = 24046
    # calculate reference number
    # generate ref number without checksum
    part_ref_number = "000000000000000000000" + str(invoice_number)
    # calculate check sum
    checksum = modulo10(part_ref_number)
    # add checksum to ref number
    return part_ref_number + str(checksum)


def qr_invoice(data, calulated_invoice_values):
    qr_invoice_var = QRBill(
        account="CH3930000ETH300011717",  # here the correct account number has to be added once available
        creditor={
            "name": "AMIV an der ETH",
            "street": "Universitätstrasse",
            "house_num": "6",
            "pcode": "8092",
            "city": "Zürich",
            "country": "CH",
        },
        debtor={
            "name": "Name",
            "street": "Strasse",
            "house_num": "Nr.",
            "pcode": "8092",
            "city": "Zürich",
            "country": "CH",
        },  # here the debitor data has to be added once part of json
        amount=calulated_invoice_values["total"],
        reference_number=calculate_ref_number(
            24046
        ),  # here the invoice number has to be added once its part of json
        language="de",
    )
    return qr_invoice_var


def qrPDF(data, calulated_invoice_values):

    # calculate all relevant data for the qr invoice
    qr_invoice_var = qr_invoice(data, calulated_invoice_values)

    # ----------------create PDF----------------
    # Create a temporary file to hold the SVG content in text mode
    with tempfile.NamedTemporaryFile(
        delete=False, suffix=".svg", mode="w+", encoding="utf-8"
    ) as temp:
        qr_invoice_var.as_svg(temp)  # Generate SVG content
        temp.seek(0)  # Move to the start of the file
        drawing = svg2rlg(temp.name)  # Convert SVG to a drawing object

    # Get the dimensions of the A4 page
    a4_width, a4_height = A4

    # Scale the drawing if necessary to fit within the A4 width
    scaling_factor = min(
        a4_width / drawing.width, 1
    )  # Ensures it fits in width, no stretching
    drawing.width *= scaling_factor
    drawing.height *= scaling_factor
    drawing.scale(scaling_factor, scaling_factor)

    # Calculate the position to place the drawing at the bottom of the A4 page
    x_offset = (a4_width - drawing.width) / 2  # Center horizontally
    y_offset = 0  # Place near the bottom with a 20mm margin

    # Create a new PDF document
    pdf_file = tempfile.NamedTemporaryFile(delete=False, suffix=".pdf")
    c = canvas.Canvas(pdf_file, pagesize=A4)

    # Translate the drawing to the bottom
    drawing.translate(x_offset, y_offset)

    # Draw the drawing onto the canvas
    renderPDF.draw(drawing, c, 0, 0)

    # Save the PDF
    c.showPage()
    c.save()
    return pdf_file
