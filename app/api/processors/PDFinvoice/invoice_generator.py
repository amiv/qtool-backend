import os
import tempfile
import uuid
from datetime import datetime, timedelta

import pdfkit
from jinja2 import Environment, FileSystemLoader
from PyPDF2 import PdfMerger

from app.api.processors.PDFinvoice.footer import add_footer_to_existing_pdf
from app.api.processors.PDFinvoice.qr_invoice_generator import qrPDF
from app.api.routes.api_helper import read_object
from app.model import *


def run_invoice_generator():
    # Ensure XDG_RUNTIME_DIR is set
    os.environ["XDG_RUNTIME_DIR"] = "/tmp/runtime-vscode"

    # Create two temporary PDF files
    temp_invoice_pdf = tempfile.NamedTemporaryFile(suffix=".pdf", delete=False)
    temp_qr_invoice_PDF = tempfile.NamedTemporaryFile(suffix=".pdf", delete=False)

    # read db data for new invoice
    data: Invoice = read_object(
        Invoice, uuid.UUID("18ea63d1-2c85-4bb2-a91d-2936cc3fa2be"), Invoice
    )
    # print(data)
    # print(data.items)
    # print(data.items[0].debitor)
    # print(data.items[0].item.description_de)

    # calculate and define additional info for invoice
    # dates
    issue_date = data.time_create.strftime("%d.%m.%Y")
    due_date = (data.time_create + timedelta(days=data.payinterval)).strftime(
        "%d.%m.%Y"
    )

    # totals
    total_exc_mwst = sum(data.items[i].amount for i in range(len(data.items))) / 100.0
    mwst = (
        sum(
            data.items[i].item.mwst_published * data.items[i].amount
            for i in range(len(data.items))
        )
        / 100.0
    )  # change later when mwst is not a string aymore: data.items[i].debitor.mwst
    total = total_exc_mwst + mwst
    # print(total_exc_mwst)
    # Merging into a dictionary and formatting the values to two decimal places
    calulated_invoice_values = {
        "issue_date": issue_date,
        "due_date": due_date,
        "total_exc_mwst": "{:.2f}".format(total_exc_mwst),
        "mwst": "{:.2f}".format(mwst),
        "total": "{:.2f}".format(total),
    }
    # Set up the environment and load the HTML template for the normal invoice pdf
    env = Environment(loader=FileSystemLoader("."))
    template = env.get_template("./app/api/processors/PDFinvoice/template.html")

    # Render the HTML template with the data
    html_out = template.render(data=data, add_data=calulated_invoice_values)

    # Optional: Write the HTML to a file (useful for debugging)
    # with open('./app/api/processors/PDFinvoice/invoice.html', 'w') as file:
    #    file.write(html_out)
    options = {
        "enable-local-file-access": None,  # Allow access to local files
    }

    # Convert the rendered HTML to PDF using pdfkit
    pdfkit.from_string(html_out, temp_invoice_pdf.name, options=options)

    # Add the footer to the PDF and a watermark if qcheck is false
    if data.qcheck == False:
        add_footer_to_existing_pdf(
            temp_invoice_pdf.name, temp_invoice_pdf.name, "Entwurf"
        )
    else:
        add_footer_to_existing_pdf(temp_invoice_pdf.name, temp_invoice_pdf.name)

    # create the qr invoice pdf based on the data
    temp_qr_invoice_PDF = qrPDF(data, calulated_invoice_values)

    # Merge the two PDFs using PdfMerger
    merger = PdfMerger()
    merger.append(temp_invoice_pdf.name)
    merger.append(temp_qr_invoice_PDF.name)

    # Save the final PDF to a new file
    final_pdf = "merged_output.pdf"
    with open(final_pdf, "wb") as f:
        merger.write(f)

    # Close and cleanup the temporary files
    merger.close()
    temp_invoice_pdf.close()
    temp_qr_invoice_PDF.close()

    print(f"PDFs merged successfully into {final_pdf}")


run_invoice_generator()
