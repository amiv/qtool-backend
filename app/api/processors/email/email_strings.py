def reimbursement_request_accepted_subject(request_id):
    return f"Reimbursement Request Accepted - ID {request_id}"


def reimbursement_request_accepted_body(
    recipient_name, request_id, description, amount
):
    return f"""\
Dear {recipient_name},

Your reimbursement request with the ID {request_id} was accepted.

Description: {description}
Amount: {amount/100}

You will receive the payment in the next few days.

The current status of all your requests is visible in QTool:
<https://qtool.amiv.ethz.ch>

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def reimbursement_request_declined_subject(request_id):
    return f"Reimbursement Request Declined - ID {request_id}"


def reimbursement_request_declined_body(
    recipient_name, request_id, decline_comment, description, amount
):
    return f"""\
Dear {recipient_name},

Your reimbursement request with the ID {request_id} was declined.

Description: {description}
Amount: {amount/100}
Reason for decline:
{decline_comment}

You can correct the mistakes and resubmit your request on the platform:
<https://qtool.amiv.ethz.ch>

The current status of all your requests is visible in QTool.

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def bill_payment_request_accepted_subject(request_id):
    return f"Bill Payment Request Accepted - ID {request_id}"


def bill_payment_request_accepted_body(recipient_name, request_id, description, amount):
    return f"""\
Dear {recipient_name},

Your bill payment request with the ID {request_id} was accepted.

Description: {description}
Amount: {amount/100}

We will issue the payment in the next few days.

The current status of all your requests is visible in QTool:
<https://qtool.amiv.ethz.ch>

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def bill_payment_request_declined_subject(request_id):
    return f"Bill Payment Request Declined - ID {request_id}"


def bill_payment_request_declined_body(
    recipient_name, request_id, decline_comment, description, amount
):
    return f"""\
Dear {recipient_name},

Your bill payment request with the ID {request_id} was declined.

Description: {description}
Amount: {amount/100}
Reason for decline:
{decline_comment}

Please ensure the required corrections are made and re-upload the receipt as soon as possible to resolve your liability for this transaction.

You can upload the corrected receipt on the platform:
<https://qtool.amiv.ethz.ch>

The current status of all your requests is visible in QTool.

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def credit_card_receipt_accepted_subject(receipt_id):
    return f"Credit Card Receipt Accepted - ID {receipt_id}"


def credit_card_receipt_accepted_body(recipient_name, receipt_id, description, amount):
    return f"""\
Dear {recipient_name},

Your credit card receipt upload with the ID {receipt_id} has been successfully accepted.

Description: {description}
Amount: {amount/100}

With this, your liability for the corresponding transaction is now cleared.

The current status of all your requests is visible in QTool:
<https://qtool.amiv.ethz.ch>

If you have any questions, you can contact us by replying to this email.

Thank you for your prompt action.

Best regards,
Amiv Treasury
"""


def credit_card_receipt_declined_subject(receipt_id):
    return f"Credit Card Receipt Declined - ID {receipt_id}"


def credit_card_receipt_declined_body(
    recipient_name, receipt_id, decline_comment, description, amount
):
    return f"""\
Dear {recipient_name},

Your credit card receipt upload with the ID {receipt_id} was declined.

Description: {description}
Amount: {amount/100}
Reason for decline:
{decline_comment}

Please ensure the required corrections are made and re-upload the receipt as soon as possible to resolve your liability for this transaction.

You can upload the corrected receipt on the platform:
<https://qtool.amiv.ethz.ch>

The current status of all your requests is visible in QTool.

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def internal_transfer_request_accepted_subject(transfer_id):
    return f"Internal Transfer Request Accepted - ID {transfer_id}"


def internal_transfer_request_accepted_body(
    recipient_name, transfer_id, description, amount
):
    return f"""\
Dear {recipient_name},

Your internal transfer request with the ID {transfer_id} has been successfully accepted.

Description: {description}
Amount: {amount/100}

The transaction is now immediately visible in QTool for your reference:
<https://qtool.amiv.ethz.ch>

The current status of all your requests is visible in QTool.

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def internal_transfer_request_declined_subject(transfer_id):
    return f"Internal Transfer Request Declined - ID {transfer_id}"


def internal_transfer_request_declined_body(
    recipient_name, transfer_id, decline_comment, description, amount
):
    return f"""\
Dear {recipient_name},

Your internal transfer request with the ID {transfer_id} was declined.

Description: {description}
Amount: {amount/100}
Reason for decline:
{decline_comment}

Please make the necessary corrections and resubmit your request on QTool:
<https://qtool.amiv.ethz.ch>

The current status of all your requests is visible in QTool.

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def invoice_issued_subject(invoice_id):
    return f"Invoice Issued - ID {invoice_id}"


def invoice_issued_body(recipient_name, invoice_id, customer_name, amount):
    return f"""\
Dear {recipient_name},

Your invoice with the ID {invoice_id} has been successfully issued.

Customer Name: {customer_name}
Amount: {amount/100}

You can now download the final version of the invoice without a watermark from QTool and send it to the customer:
<https://qtool.amiv.ethz.ch>

The current status of all your requests is visible in QTool.

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def invoice_declined_subject(invoice_id):
    return f"Invoice Declined - ID {invoice_id}"


def invoice_declined_body(
    recipient_name, invoice_id, decline_comment, customer_name, amount
):
    return f"""\
Dear {recipient_name},

Your invoice with the ID {invoice_id} has been declined.

Customer Name: {customer_name}
Amount: {amount/100}
Reason for decline:
{decline_comment}

Please correct the issue and issue a new invoice through QTool:
<https://qtool.amiv.ethz.ch>

The current status of all your requests is visible in QTool.

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def reimbursement_in_payment_subject(request_id):
    return f"Reimbursement in Payment - ID {request_id}"


def reimbursement_in_payment_body(recipient_name, request_id, description, amount):
    return f"""\
Dear {recipient_name},

Your reimbursement request with the ID {request_id} is now in payment.

Description: {description}
Amount: {amount/100}

The payment is being processed and should be completed soon. The current status of all your requests is visible in QTool:
<https://qtool.amiv.ethz.ch>

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def bill_in_payment_subject(request_id):
    return f"Bill in Payment - ID {request_id}"


def bill_in_payment_body(recipient_name, request_id, description, amount):
    return f"""\
Dear {recipient_name},

Your bill payment request with the ID {request_id} is now in payment.

Description: {description}
Amount: {amount/100}

The payment is being processed and should be completed soon. The current status of all your requests is visible in QTool:
<https://qtool.amiv.ethz.ch>

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""


def customer_payment_received_subject(invoice_id):
    return f"Customer Payment Received - Invoice ID {invoice_id}"


def customer_payment_received_body(recipient_name, invoice_id, customer_name, amount):
    return f"""\
Dear {recipient_name},

We have received a notification that the customer has paid the invoice with the ID {invoice_id}.

Customer Name: {customer_name}
Amount: {amount/100}

The payment has been registered in the system. The current status of all your requests is visible in QTool:
<https://qtool.amiv.ethz.ch>

If you have any questions, you can contact us by replying to this email.

Best regards,
Amiv Treasury
"""
