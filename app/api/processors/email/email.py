import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from app.core.config import settings


def mail(to, subject, text, html=None):
    """Send a mail to a list of recipients.

    The mail is sent from the address specified by API_MAIL in the config,
    and the subject formatted according to the subject argument.


    Args:
        to(list of strings): List of recipient addresses
        subject(string): Subject string
        text(string): Mail content as plaintext
        html(string): Mail content as HTML
        reply_to(string): Address of the reply-to field
    """
    sender_address = settings.API_MAIL_ADDRESS
    sender_name = settings.API_MAIL_NAME
    sender = f"{sender_name} <{sender_address}>"

    if settings.TESTING:
        mail = {
            "subject": settings.API_MAIL_SUBJECT_PREFIX + subject,
            "from": sender,
            "receivers": to + "@ethz.ch;",
            "text": text,
            "html": html,
        }

        mail["reply-to"] = settings.REPLY_TO

        settings.test_mails.append(mail)
        print("testmail", mail)

    elif settings.SMTP_SERVER and settings.SMTP_PORT:
        if html is not None:
            msg = MIMEMultipart("mixed")
            msg_body = MIMEMultipart("alternative")
            msg_body.attach(MIMEText(text, "plain"))
            msg_body.attach(MIMEText(html, "html"))
            msg.attach(msg_body)
        else:
            msg = MIMEMultipart("mixed")
            msg.attach(MIMEText(text))

        msg["Subject"] = settings.API_MAIL_SUBJECT_PREFIX + subject
        msg["From"] = sender
        msg["To"] = to + "@ethz.ch"

        msg["reply-to"] = settings.REPLY_TO

        try:
            with smtplib.SMTP(
                settings.SMTP_SERVER,
                port=settings.SMTP_PORT,
                timeout=settings.SMTP_TIMEOUT,
            ) as smtp:
                status_code, _ = smtp.starttls()
                if status_code != 220:
                    print("Failed to create secure " "SMTP connection!")
                    return

                if settings.SMTP_USERNAME and settings.SMTP_PASSWORD:
                    smtp.login(settings.SMTP_USERNAME, settings.SMTP_PASSWORD)
                else:
                    smtp.ehlo()

                try:
                    smtp.sendmail(sender_address, msg["To"], msg.as_string())
                except smtplib.SMTPRecipientsRefused as e:
                    print("Recipient refused by server: %s" % e)
                    print(msg.as_string())
                    error = (
                        "Failed to send mail:\n"
                        "From: %s\nTo: %s\n"
                        "Subject: %s\n\n%s"
                    )
                    print(error % (sender, str(to), subject, text))
        except smtplib.SMTPException as e:
            print("SMTP error trying to send mails: %s" % e)
