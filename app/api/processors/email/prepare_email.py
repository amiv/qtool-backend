import app.api.processors.email.email_strings as email_strings
from app.api.processors.email.email import mail


def send_reimbursement_accepted_email(request_id, recipient_name, description, amount):
    subject = email_strings.reimbursement_request_accepted_subject(request_id)
    body = email_strings.reimbursement_request_accepted_body(
        recipient_name, request_id, description, amount
    )
    mail(recipient_name, subject, body)


def send_reimbursement_declined_email(request_id, recipient_name, description, amount):
    subject = email_strings.reimbursement_request_declined_subject(request_id)
    body = email_strings.reimbursement_request_declined_body(
        recipient_name, request_id, description, "asdf", amount
    )
    mail(recipient_name, subject, body)


def send_bill_payment_accepted_email(request_id, recipient_name, description, amount):
    subject = email_strings.bill_payment_request_accepted_subject(request_id)
    body = email_strings.bill_payment_request_accepted_body(
        recipient_name, request_id, description, amount
    )
    mail(recipient_name, subject, body)


def send_bill_payment_declined_email(request_id, recipient_name, description, amount):
    subject = email_strings.bill_payment_request_declined_subject(request_id)
    body = email_strings.bill_payment_request_declined_body(
        recipient_name, request_id, description, "asdf", amount
    )
    mail(recipient_name, subject, body)


def send_credit_card_accepted_email(request_id, recipient_name, description, amount):
    subject = email_strings.credit_card_receipt_accepted_subject(request_id)
    body = email_strings.credit_card_receipt_accepted_body(
        recipient_name, request_id, description, amount
    )
    mail(recipient_name, subject, body)


def send_credit_card_declined_email(request_id, recipient_name, description, amount):
    subject = email_strings.credit_card_receipt_declined_subject(request_id)
    body = email_strings.credit_card_receipt_declined_body(
        recipient_name, request_id, description, "asdf", amount
    )
    mail(recipient_name, subject, body)


def send_internal_transfer_accepted_email(
    request_id, recipient_name, description, amount
):
    subject = email_strings.internal_transfer_request_accepted_subject(request_id)
    body = email_strings.internal_transfer_request_accepted_body(
        recipient_name, request_id, description, amount
    )
    mail(recipient_name, subject, body)


def send_internal_transfer_declined_email(
    request_id, recipient_name, description, amount
):
    subject = email_strings.internal_transfer_request_declined_subject(request_id)
    body = email_strings.internal_transfer_request_declined_body(
        recipient_name, request_id, description, "asdf", amount
    )
    mail(recipient_name, subject, body)
