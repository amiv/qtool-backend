import uuid
from datetime import datetime
from typing import List, Optional, Union

from sqlmodel import Field, Relationship, SQLModel

from app.models.addresses import (
    Address,
    AddressBase,
    AddressesPublic,
    AddressFilter,
    AddressPublic,
)
from app.models.bills import (
    Bill,
    BillBase,
    BillCreate,
    BillFilter,
    BillPublic,
    BillsList,
)
from app.models.creditor import Creditor, CreditorBase, CreditorPublic
from app.models.creditPayments import (
    CreditPayment,
    CreditPaymentBase,
    CreditPaymentCreate,
    CreditPaymentFilter,
    CreditPaymentPublic,
    CreditPaymentsList,
)
from app.models.debitor import Debitor, DebitorBase, DebitorPublic
from app.models.ezags import Ezag, EzagBase
from app.models.internalTransfers import (
    InternalTransfer,
    InternalTransferBase,
    InternalTransferCreate,
    InternalTransferFilter,
    InternalTransferPublic,
    InternalTransfersList,
)
from app.models.invoices import (
    Invoice,
    InvoiceBase,
    InvoiceCreate,
    InvoiceFilter,
    InvoicePublic,
    InvoicesList,
    ItemstoInvoice,
    ItemstoInvoiceCreate,
    ItemstoInvoicePublic,
)
from app.models.items import Item, ItemBase, ItemFilter, ItemPublic, ItemsPublic
from app.models.ksts import Kst, KstBase, KstFilter, KstPublic, KstsPublic
from app.models.ledgers import (
    Ledger,
    LedgerBase,
    LedgerFilter,
    LedgerPublic,
    LedgersPublic,
)
from app.models.reimbursements import (
    Reimbursement,
    ReimbursementBase,
    ReimbursementCreate,
    ReimbursementFilter,
    ReimbursementPublic,
    ReimbursementsList,
)
from app.models.unlinked_files import UnlinkedFiles
from app.models.Users import (
    DbUser,
    DbUserBase,
    DbUserCreate,
    DbUserFilter,
    DbUserPublic,
    DbUsersList,
)

AllDbModels = Union[
    Address,
    Bill,
    Ezag,
    Invoice,
    Item,
    Kst,
    Ledger,
    Debitor,
    Creditor,
    InternalTransfer,
    Reimbursement,
    CreditPayment,
    UnlinkedFiles,
    DbUser,
]


AllModelsBase = Union[
    AddressBase,
    BillBase,
    EzagBase,
    InvoiceBase,
    ItemBase,
    KstBase,
    LedgerBase,
    DebitorBase,
    CreditorBase,
    InternalTransferBase,
    ReimbursementBase,
    CreditPaymentBase,
    DbUserBase,
]
AllModelsPublic = Union[
    ItemPublic,
    BillPublic,
    InvoicePublic,
    DebitorPublic,
    CreditorPublic,
    KstPublic,
    LedgerPublic,
    AddressPublic,
    InternalTransferPublic,
    ReimbursementPublic,
    CreditPaymentPublic,
    DbUserPublic,
]
AllModelsCreate = Union[
    BillCreate,
    InvoiceCreate,
    ItemstoInvoiceCreate,
    InternalTransferCreate,
    ReimbursementCreate,
    ItemBase,
    DebitorBase,
    CreditorBase,
    KstBase,
    LedgerBase,
    AddressBase,
    CreditPaymentCreate,
    DbUserCreate,
]
AllModelsFilter = Union[
    ItemFilter,
    LedgerFilter,
    KstFilter,
    AddressFilter,
    BillFilter,
    InvoiceFilter,
    ReimbursementFilter,
    InternalTransferFilter,
    CreditPaymentFilter,
    CreditPaymentFilter,
    DbUserFilter,
]
AllModelsList = Union[
    ItemsPublic,
    LedgersPublic,
    KstsPublic,
    AddressesPublic,
    InvoicesList,
    BillsList,
    ReimbursementsList,
    InternalTransfersList,
    CreditPaymentsList,
    DbUsersList,
]
