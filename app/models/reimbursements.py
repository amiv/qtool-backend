import uuid
from datetime import datetime
from typing import Optional

import uuid6
from sqlmodel import Field, Relationship, SQLModel

from app.models.addresses import Address, AddressBase, AddressPublic
from app.models.creditor import Creditor, CreditorBase, CreditorFilter, CreditorPublic
from app.models.filter import BaseFilter


class ReimbursementBase(SQLModel):
    creditor_id: uuid.UUID = Field(foreign_key="creditor.id")
    receipt: uuid.UUID = Field()  # TODO: change to receipt_id foreign key


class Reimbursement(ReimbursementBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)
    ezag_id: uuid.UUID | None = Field(default=None)
    ezag_timestamp: datetime | None = Field(default=None)
    check_timestamp: datetime | None = Field(default=None)
    recipient: str = Field(max_length=30)  # TODO: change to recipient_id foreign key
    creditor: "Creditor" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})


class ReimbursementCreate(SQLModel):
    creditor: CreditorBase
    receipt: uuid.UUID
    recipient: str


class ReimbursementPublic(ReimbursementBase):
    id: uuid.UUID
    ezag_id: uuid.UUID | None
    ezag_timestamp: datetime | None
    creditor: CreditorPublic
    receipt: str | uuid.UUID
    recipient: str


class ReimbursementsList(SQLModel):
    items: list[ReimbursementPublic]
    count: int
    total: int


class ReimbursementFilter(BaseFilter):
    creditor: Optional[CreditorFilter] = None
    ezag_timestamp: datetime | None = None
    ezag_id: uuid.UUID | None = None
    receipt: str | None = None
    recipient: str | None = None

    class Constants:
        model = Reimbursement
        search_field_name = "search"
