import uuid
from datetime import datetime
from typing import Optional

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel

from app.models.filter import BaseFilter


class AddressBase(SQLModel):
    name: str = Field(max_length=30)
    address1: str = Field(max_length=30)
    address2: str = Field(max_length=30)
    address3: str = Field(max_length=30)
    plz: str = Field(max_length=10, min_length=4)
    city: str = Field(max_length=30, min_length=3)
    country: str = Field(max_length=30, min_length=2)


class Address(AddressBase):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)


class UserAddress(Address, table=True):
    pass


class BillAddress(Address, table=True):
    pass


class InvoiceAddress(Address, table=True):
    pass


class AddressPublic(AddressBase):
    id: uuid.UUID = Field()


class AddressesPublic(SQLModel):
    items: list[AddressPublic] = []
    count: int
    total: int


class AddressFilter(BaseFilter):
    name: str | None = None
    address1: str | None = None
    address2: str | None = None
    address3: str | None = None
    city: str | None = None
    country: str | None = None
    plz: int | None = None
    search: Optional[str] = None

    class Constants:
        model = Address
        search_field_name = "search"
