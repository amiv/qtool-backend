import uuid
from datetime import datetime
from typing import Optional

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel

from app.models.filter import BaseFilter


class ItemBase(SQLModel):
    title_de: str = Field(min_length=1, max_length=255)
    description_de: str = Field(max_length=255)
    title_en: str = Field(min_length=1, max_length=255)
    description_en: str = Field(max_length=255)
    price: int = Field(gt=4)
    unit: str = Field(max_length=30)
    mwst: float = Field(ge=0, le=100)
    mwst_type: str = Field(max_length=255)
    mwst_published: float = Field(ge=0, le=100)
    active: bool = Field(default=True)


class Item(ItemBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)


class ItemPublic(ItemBase):
    id: uuid.UUID = Field()


class ItemsPublic(SQLModel):
    items: list[ItemPublic] = []
    count: int
    total: int


class ItemFilter(BaseFilter):
    price: Optional[int] = None
    unit: Optional[str] = None
    active: Optional[bool] = None
    search: Optional[str] = None

    class Constants(Filter.Constants):
        model = Item
        search_field_name = "search"
        search_model_fields = ["name_de", "name_en", "description_de", "description_en"]
