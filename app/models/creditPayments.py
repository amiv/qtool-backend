import inspect
import uuid
from datetime import datetime
from enum import Enum
from typing import Optional

import uuid6
from sqlmodel import Field, Relationship, SQLModel

from app.models.addresses import Address, AddressBase, AddressFilter, AddressPublic
from app.models.creditor import Creditor, CreditorBase, CreditorFilter, CreditorPublic
from app.models.filter import BaseFilter


class Card(str, Enum):
    Event = "Events"
    President = "President"
    Quaestor = "Quaestor"


class CreditPaymentBase(SQLModel):
    creditor_id: uuid.UUID = Field(foreign_key="creditor.id")
    receipt: uuid.UUID = Field()  # TODO: change to receipt_id foreign key
    card: Card = Field()


class CreditPayment(CreditPaymentBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)
    ezag_timestamp: datetime | None = Field(default=None)
    creditor: "Creditor" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})


class CreditPaymentCreate(SQLModel):
    creditor: CreditorBase
    receipt: uuid.UUID
    card: Card


class CreditPaymentPublic(CreditPaymentBase):
    id: uuid.UUID
    creditor: CreditorPublic
    receipt: str | uuid.UUID
    card: Card


class CreditPaymentsList(SQLModel):
    items: list[CreditPaymentPublic]
    count: int
    total: int


class CreditPaymentFilter(BaseFilter):
    """
    Filter class for CreditPayment.
    """

    receipt: Optional[str] = None
    card: Optional[str] = None
    creditor: Optional[CreditorFilter] = None  # Nested filter
    search: Optional[str] = None

    class Constants:
        model = CreditPayment  # Replace with your CreditPayment SQLModel class
        search_field_name = "search"
