import uuid
from datetime import datetime
from typing import Optional

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from schwifty import IBAN
from sqlmodel import Field, Relationship, SQLModel

from app.models.addresses import (
    Address,
    AddressBase,
    AddressFilter,
    AddressPublic,
    BillAddress,
)
from app.models.creditor import Creditor, CreditorBase, CreditorFilter, CreditorPublic
from app.models.filter import BaseFilter


class BillBase(SQLModel):
    creditor_id: uuid.UUID = Field(foreign_key="creditor.id")
    iban: Optional[IBAN] = Field(default=None, max_length=34)
    address_id: uuid.UUID = Field(foreign_key="billaddress.id")
    reference: str = Field(max_length=50)
    receipt: uuid.UUID = Field()


class Bill(BillBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)
    ezag_timestamp: datetime | None = Field(default=None)
    check_timestamp: datetime | None = Field(default=None)
    # creator_id: uuid.UUID = Field(foreign_key="user.id")
    creditor: "Creditor" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})
    address: "BillAddress" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})


class BillCreate(SQLModel):
    creditor: CreditorBase
    address: AddressBase
    reference: str
    iban: IBAN
    receipt: uuid.UUID


class BillPublic(BillBase):
    id: uuid.UUID
    creditor: CreditorPublic
    address: AddressPublic
    receipt: str | uuid.UUID


class BillsList(SQLModel):
    items: list[BillPublic]
    count: int
    total: int


class BillFilter(BaseFilter):
    # number_filter: Optional[Filter] = FilterDepends(with_prefix("address", AddressFilter))
    creditor: Optional[CreditorFilter] = None
    address: Optional[AddressFilter] = None
    reference: str | None = None
    iban: str | None = None
    receipt: str | None = None

    class Constants:
        model = Bill
        search_field_name = "search"
