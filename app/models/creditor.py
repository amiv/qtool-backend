import uuid
from datetime import datetime
from enum import Enum
from typing import Optional

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from sqlmodel import Field, Relationship, SQLModel

from app.models.filter import BaseFilter
from app.models.ksts import Kst, KstFilter
from app.models.ledgers import Ledger, LedgerFilter
from app.models.Users import DbUser


# create a currency enum
class Currency(str, Enum):
    CHF = "CHF"
    EUR = "EUR"
    USD = "USD"


class q_state(str, Enum):
    open = "open"
    accepted = "accepted"
    rejected = "rejected"


class CreditorBase(SQLModel):
    kst_id: uuid.UUID = Field(foreign_key="kst.id")
    ledger_id: uuid.UUID = Field(foreign_key="ledger.id")
    amount: int = Field(gt=0, le=100000000)
    accounting_year: int = Field(default_factory=lambda: datetime.now().year)
    currency: Currency = Field()
    comment: str = Field(min_length=3, max_length=255)
    qcomment: str = Field(default="", max_length=255)
    q_check: q_state = Field(default=q_state.open)
    q_check_timestamp: datetime | None = Field(default=None)
    creator_id: str = Field(foreign_key="dbuser.id", max_length=40)
    name: str = Field(max_length=255)


class Creditor(CreditorBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)
    ledger: "Ledger" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})
    kst: "Kst" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})
    creator: "DbUser" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})

    def namefunction(self):
        print(self)
        self.name = (
            f"{self.kst.kst_number}-"
            f"{self.time_create.strftime('%y%m%d:%H%M%S')}-"
            f"{str(self.id)[-4:]}"
        )


class CreditorPublic(CreditorBase):
    id: uuid.UUID = Field()
    time_create: datetime
    time_modified: datetime
    # kst: Kst | None = None


class CreditorFilter(BaseFilter):
    kst: Optional[KstFilter] = None
    ledger: Optional[LedgerFilter] = None
    amount: int | None = None
    accounting_year: int | None = None
    currency: Currency | None = None
    comment: str | None = None
    qcomment: str | None = None
    creator: str | None = None
    name: str | None = None
    q_check: q_state | None = None
    q_check_timestamp: datetime | None = None

    class Constants(Filter.Constants):
        model = Creditor
        search_field_name = "search"
