import uuid
from typing import Any, Dict, List, Optional, Type, Union, get_args, get_origin

from fastapi import HTTPException
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import BaseModel
from pydantic import Field as pydantic_Field
from pydantic import create_model
from sqlalchemy import BOOLEAN, Boolean, ColumnElement, String, asc, desc
from sqlalchemy.orm import InstrumentedAttribute
from sqlmodel import SQLModel, cast, or_


class BaseFilter(SQLModel):
    """
    Base filter class to provide dynamic filtering and search functionality.
    """

    search: Optional[str] = None
    sort: Optional[str] = None

    class Constants:
        model: Type[SQLModel] = None  # To be set in derived classes
        search_field_name = "search"

    def resolve_field_type(self, field_annotation):
        """
        Resolves the actual type from field annotations, handling Optional/Union types.
        """
        if get_origin(field_annotation) is Union:
            # If the field is Optional[Type], extract the non-None type
            args = get_args(field_annotation)
            return next(arg for arg in args if arg is not type(None))
        return field_annotation

    def flatten_pydantic_model(
        self, model: Type[BaseModel], parent_key: str = "", sep: str = "__"
    ) -> Dict[str, Any]:
        """
        Recursively flattens a Pydantic model's fields using dot notation for nested fields.

        Args:
            model (Type[BaseModel]): The Pydantic model to flatten.
            parent_key (str, optional): The base key string for nested fields. Defaults to ''.
            sep (str, optional): The separator between parent and child keys. Defaults to '.'.

        Returns:
            Dict[str, Any]: A dictionary mapping flattened field names to their types and defaults.
        """
        # print(type(self))
        fields = {}
        for field_name, field in model.model_fields.items():
            # Generate the full key for flattened fields
            full_key = f"{parent_key}{sep}{field_name}" if parent_key else field_name

            # Resolve the field type (handle Optional, Union)
            field_type = self.resolve_field_type(field.annotation)

            if isinstance(field_type, type) and issubclass(field_type, BaseModel):
                # Recursively flatten nested Pydantic models
                nested_fields = self.flatten_pydantic_model(
                    field_type, parent_key=full_key, sep=sep
                )
                fields.update(nested_fields)
            else:
                fields[full_key] = (
                    Optional[field_type],
                    pydantic_Field(None, alias=full_key),
                )
        return fields

    def create_flattened_model(self) -> Type[BaseModel]:
        """
        Creates a flattened Pydantic model with dot notation aliases for nested fields.

        Args:
            name (str): The name of the new flattened model.

        Returns:
            Type[BaseModel]: The new flattened Pydantic model.
        """
        flattened_fields = self.flatten_pydantic_model(self.__class__)
        flattened_model = create_model(
            "flattened filter", **flattened_fields, __base__=self.__class__
        )
        # print(flattened_model.model_fields)
        return flattened_model

    def filter(self, query):
        """
        Applies filtering logic to a given SQLModel query, including nested filters and search.
        """
        self_model = self.Constants.model
        filter_data = self.model_dump(exclude_unset=True)
        join_paths = set()
        print(filter_data)
        print(f"model: {self_model.model_fields}")
        for field_name, field_value in filter_data.items():
            if field_value is None:
                continue

            if field_name == "search":
                continue  # Handle search separately later
            if field_name == "sort":
                continue

            field_name = field_name.replace("__", ".")  # Support nested fields
            field_path = field_name.split(".")
            fields = self_model.__fields__
            model_field = getattr(self_model, field_path[0], None)
            if model_field is None:
                model_field = fields.get(field_path[0], None)
            print(
                f"model_field: {model_field}, field_path: {field_path}, field_value: {field_value}"
            )
            # Build joins for nested fields
            for path in field_path[1:]:
                if isinstance(model_field, InstrumentedAttribute):
                    model_field = model_field.property.mapper.class_
                    model_field = getattr(model_field, path, None)
                    join_paths.add(path)
                else:
                    model_field = getattr(model_field, path, None)

            if model_field is None:
                continue  # Skip non-existent fields

            column = getattr(self_model, field_path[0], None)
            for path in field_path[1:]:
                if isinstance(column, InstrumentedAttribute):
                    query = query.join(column)
                    column = column.property.mapper.class_
                    column = getattr(column, path, None)
            print(column.type)
            if str(column.type) == "STRING":
                query = query.filter(column.contains(f"%{field_value}%"))
            elif str(column.type) == "BOOLEAN":
                query = query.filter(column == field_value)
            else:
                query = query.filter(cast(column, String).contains(f"%{field_value}%"))
        # Handle global search
        if self.search:
            search_conditions = self._build_search_conditions(self_model, self.search)
            if search_conditions:
                # print(f"joins: {self._build_search_joins(model, self.search)}")
                for join_path in self._build_search_joins(self_model, self.search):
                    # Join the path if not already joined
                    query = query.join(join_path)
                print(
                    f"search_conditions: {[cond.all_() for cond in search_conditions]}"
                )
                # print(f"search_conditions: {[cond.all_() for cond in search_conditions]}")
                query = query.filter(or_(*search_conditions))
        if self.sort:
            for join_path in self._build_search_joins(self_model, self.search):
                # Join the path if not already joined
                query = query.join(join_path)
            query = self._apply_sorting(query, self_model)

        return query

    def _resolve_column(self, model, field_path, join_paths, query):
        """
        Resolves the column for a nested field path and ensures joins are added.
        """
        column = getattr(model, field_path[0], None)
        for path in field_path[1:]:
            if isinstance(column, InstrumentedAttribute):
                query = query.join(column)
                column = column.property.mapper.class_
                column = getattr(column, path, None)
        return column

    def _apply_sorting(self, query, model):
        """
        Dynamically applies sorting logic to the query based on the `sort` field.

        Example sort string: "creditor__amount:desc"
        """
        sort_param = self.sort.strip()
        field_order = "asc"  # Default order

        # Extract field path and order
        if ":" in sort_param:
            sort_field, field_order = sort_param.split(":")
            field_order = field_order.lower()
        else:
            sort_field = sort_param

        # Resolve nested field path
        sort_field = sort_field.replace("__", ".")
        field_path = sort_field.split(".")

        column = self._resolve_column(model, field_path, set(), query)
        if not column:
            raise HTTPException(405, f"Invalid sorting field: {sort_field}")

        # Apply sorting order
        if field_order == "desc":
            query = query.order_by(desc(column))
        else:
            query = query.order_by(asc(column))

        return query

    def _build_search_joins(
        self, model: SQLModel, search_value: str
    ) -> List[InstrumentedAttribute]:
        """
        Dynamically creates a list of relationship attributes to be joined for searching.

        This method inspects the given model and its related models recursively.
        For each relationship found, it will add the corresponding column to a list
        of joins so that conditions on nested fields can be applied.

        Args:
            model: The root SQLModel class to inspect.
            search_value: The search string.

        Returns:
            A list of relationship columns that need to be joined.
        """
        search_joins: List[InstrumentedAttribute] = []
        visited_models: set[Type[SQLModel]] = set()

        def process_model(m: Type[SQLModel]):
            # Prevent infinite recursion if cyclic relationships exist
            if m in visited_models:
                return
            visited_models.add(m)

            for field_name, column in m.__dict__.items():
                # Check if this is an ORM mapped attribute
                if isinstance(column, InstrumentedAttribute):
                    # Determine if column is a relationship or a simple column
                    # Relationships have a `.property.mapper` attribute
                    # print(f"column: {column}")
                    prop = getattr(column, "property", None)
                    # print(f"prop: {prop}, prop.mapper: {getattr(prop, 'mapper', None)}")
                    if prop and hasattr(prop, "mapper"):
                        # It's a relationship field
                        # print(f"related_model: {prop.mapper.class_}")
                        related_model = prop.mapper.class_
                        # Add to joins for later
                        search_joins.append(column)
                        # Recurse into related model
                        process_model(related_model)

        # Start from the root model
        process_model(model)
        return search_joins

    def _build_search_conditions(
        self, model: SQLModel, search_value: str
    ) -> List[ColumnElement]:
        """
        Dynamically creates search conditions for all string-like fields,
        including nested fields.
        """
        search_conditions = []

        def process_model(model, parent_alias=None, prefix=""):
            """
            Recursively process a model to extract string and numeric columns for search.
            Handles nested relationships using joins.
            """
            for field_name, column in model.__dict__.items():
                # Check for valid ORM attributes
                if isinstance(column, InstrumentedAttribute):
                    try:
                        # Resolve the column type
                        column_type = column.type
                        # print(f"column: {column} column_type: {column_type}")
                        if column_type is None:
                            continue
                        try:
                            if hasattr(column_type, "python_type"):
                                column_type = column_type.python_type
                        #        print(f"column_type: {column_type}")
                        except NotImplementedError:
                            pass
                        # Add string-like fields to search conditions
                        if column_type == str:
                            # print(f"str column: {column}")
                            search_conditions.append(
                                column.contains(f"%{search_value}%")
                            )

                        # Add numeric fields, casting to string
                        elif column_type in [int, float]:
                            # print(f"int column: {column}")
                            search_conditions.append(
                                cast(column, String).contains(f"%{search_value}%")
                            )
                        elif column_type == uuid.UUID:
                            # skip uuid fields
                            pass
                        else:
                            search_conditions.append(
                                cast(column, String).contains(f"%{search_value}%")
                            )
                            # print(f"elsecolumn: {column}")
                    except AttributeError:
                        # Handle relationships or mappers
                        related_model = (
                            column.property.mapper.class_
                            if hasattr(column, "property")
                            else None
                        )
                        if related_model:
                            # Recurse into related model
                            process_model(
                                related_model, column, prefix=f"{prefix}{field_name}__"
                            )

        # Start processing from the root model
        process_model(model)
        return search_conditions
