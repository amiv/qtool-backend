import uuid
from datetime import datetime
from typing import Optional

from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from schwifty import IBAN
from sqlmodel import Field, Relationship, SQLModel

from app.models.addresses import (
    Address,
    AddressBase,
    AddressFilter,
    AddressPublic,
    UserAddress,
)


class DbUserBase(SQLModel):
    id: str = Field(max_length=32, primary_key=True)
    address_id: uuid.UUID = Field(foreign_key="useraddress.id")
    nethz: str = Field(max_length=100)
    iban: IBAN = Field(max_length=34)


class DbUser(DbUserBase, table=True):
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)
    address: "UserAddress" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})


class DbUserCreate(SQLModel):
    id: str
    address: AddressBase
    iban: str


class DbUserPublic(DbUserBase):
    address: AddressPublic


class DbUsersList(SQLModel):
    items: list[DbUserPublic]
    count: int
    total: int


class BasicUser(SQLModel):
    id: str
    nethz: str
    firstname: str
    lastname: str
    email: EmailStr
    role: Optional[str] = None


class DbUserFilter(Filter):
    id: str | None = None
    address: AddressFilter | None = None
    iban: IBAN | None = None
    nethz: str | None = None

    class Constants:
        model = DbUser


class User(DbUserPublic):
    """
    {
                "_id": "5d8086bee61dce5c6bc1a8b7",
                "nethz": "cwalter",
                "legi": "19919935",
                "firstname": "Clemens",
                "lastname": "Walter",
                "email": "cwalter@ethz.ch",
                "gender": "male",
                "department": "itet",
                "membership": "regular",
                "rfid": "342004",
                "phone": null,
                "send_newsletter": true,
                "_updated": "2024-11-12T05:01:39Z",
                "_created": "2019-09-17T07:09:50Z",
                "_etag": "05a22148fe715f33458a0c0c3fc4002265d9b76c",
                "password_set": true
        }
    """

    id: str
    nethz: str
    firstname: str
    lastname: str
    email: EmailStr
    role: Optional[str] = None
    groups: list[str] = []
