# store all the ids of files that aren't linked to any transaction
import uuid
from datetime import datetime

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel


class UnlinkedFiles(SQLModel, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    name: str = Field(max_length=200)
    creator: str = Field(max_length=30)  # TODO: change to user_id foreign key
