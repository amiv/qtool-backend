import uuid
from datetime import datetime

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel

from app.model import *
from app.models.creditor import q_state
from app.models.filter import BaseFilter
from app.models.items import Item, ItemPublic
from app.models.ksts import Kst, KstFilter
from app.models.ledgers import Ledger, LedgerFilter


class DebitorBase(SQLModel):
    kst_id: uuid.UUID = Field(foreign_key="kst.id")
    ledger_id: uuid.UUID = Field(foreign_key="ledger.id")
    creator_id: str = Field(foreign_key="dbuser.id", max_length=40)
    accounting_year: int = Field(default=datetime.now().year, ge=2000, le=2100)
    amount: int = Field(gt=0, le=100000000)
    q_check: q_state = Field(default=q_state.open)
    q_check_timestamp: datetime | None = Field(default=None)
    comment: str = Field(min_length=5, max_length=255)
    q_comment: str = Field(default="", max_length=255)
    name: str = Field(max_length=255)


class Debitor(DebitorBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)
    kst: "Kst" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})
    ledger: "Ledger" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})
    creator: "DbUser" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})

    def namefunction(self):
        print(self)
        self.name = (
            f"{self.kst.kst_number}-"
            f"{self.time_create.strftime('%y%m%d:%H%M%S')}-"
            f"{str(self.id)[-4:]}"
        )


class DebitorPublic(DebitorBase):
    id: uuid.UUID
    creator_id: str
    # kst: Kst | None = None
    time_create: datetime = Field(default=datetime.now())
    time_modified: datetime = Field(default=datetime.now())


class DebitorFilter(BaseFilter):
    kst: Optional[KstFilter] = None
    ledger: Optional[LedgerFilter] = None
    mwst: str | None = None

    class Constants:
        model = Debitor
        search_field_name = "search"
