import uuid
from datetime import datetime

import uuid6
from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel

from app.models.filter import BaseFilter


class EzagBase(SQLModel):
    name: str = Field(max_length=30)
    size: int = Field()


class Ezag(EzagBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)


class EzagPublic(EzagBase):
    id: uuid.UUID = Field()
    time_create: datetime = Field()
    link: str = Field()


class EzagList(SQLModel):
    items: list[Ezag]
    count: int
    total: int


class EzagPublicList(SQLModel):
    items: list[EzagPublic]
    count: int
    total: int


class EzagFilter(BaseFilter):
    name: str | None = None
    time_create: datetime | None = None
    time_modified: datetime | None = None

    class Constants:
        model = Ezag
