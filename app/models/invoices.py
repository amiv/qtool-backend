import uuid
from datetime import datetime
from enum import Enum
from typing import Optional

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel

from app.models.addresses import Address, AddressFilter, InvoiceAddress
from app.models.creditor import q_state
from app.models.debitor import Debitor, DebitorBase, DebitorFilter, DebitorPublic
from app.models.filter import BaseFilter
from app.models.items import Item, ItemPublic
from app.models.ksts import KstFilter


class MwstType(str, Enum):
    INKL = "inkl"
    EXKL = "exkl"


class ItemstoInvoice(SQLModel, table=True):
    item_id: uuid.UUID = Field(foreign_key="item.id", primary_key=True)
    invoice_id: uuid.UUID = Field(foreign_key="invoice.id", primary_key=True)
    count: float = Field(gt=0)
    amount: int = Field(gt=0, le=100000000)
    comment: str = Field(default="", max_length=255)

    # Define the relationships
    invoice: "Invoice" = Relationship(
        back_populates="items", sa_relationship_kwargs={"lazy": "selectin"}
    )
    item: "Item" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})


class ItemstoInvoicePublic(SQLModel):
    item: ItemPublic
    amount: int
    count: float
    comment: str


class ItemstoInvoiceCreate(SQLModel):
    item_id: uuid.UUID
    count: float
    amount: int
    comment: str


class InvoiceBase(SQLModel):
    debitor_id: uuid.UUID = Field(foreign_key="debitor.id")
    address_id: uuid.UUID = Field(foreign_key="invoiceaddress.id")
    invoice_date: datetime = Field(default=datetime.now())
    payinterval: int = Field(default=30)
    text_comment: str = Field(default="", max_length=1000)
    mwst_type: MwstType = Field()
    our_reference: str = Field(default="", max_length=255)
    your_reference: str = Field(default="", max_length=255)


class Invoice(InvoiceBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    items: list[ItemstoInvoice] = Relationship(
        back_populates="invoice", sa_relationship_kwargs={"lazy": "selectin"}
    )
    address: "InvoiceAddress" = Relationship(
        sa_relationship_kwargs={"lazy": "selectin"}
    )
    debitor: "Debitor" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})


class InvoiceCreate(InvoiceBase):
    items: list[ItemstoInvoiceCreate]
    debitor: DebitorBase
    address: InvoiceAddress


class InvoicePublic(InvoiceBase):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    debitor_id: uuid.UUID
    debitor: DebitorPublic
    address: InvoiceAddress
    items: list[ItemstoInvoicePublic]


class InvoicesList(SQLModel):
    items: list[InvoicePublic]
    count: int
    total: int


class InvoiceFilter(BaseFilter):
    kst: Optional[KstFilter] = None
    mwst: str | None = None
    payinterval: int | None = None
    comment: str | None = None
    debitor: Optional[DebitorFilter] = None
    time_create: datetime | None = None
    time_modified: datetime | None = None
    # creator_id: uuid.UUID
