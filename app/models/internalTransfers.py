import uuid
from datetime import datetime
from typing import Optional

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel

from app.model import Creditor, CreditorBase, Debitor, DebitorBase
from app.models.creditor import CreditorFilter, CreditorPublic
from app.models.debitor import DebitorFilter, DebitorPublic
from app.models.filter import BaseFilter


class InternalTransferBase(SQLModel):
    comment: str = Field(default="", max_length=255)
    debitor_id: uuid.UUID = Field(foreign_key="debitor.id")
    creditor_id: uuid.UUID = Field(foreign_key="creditor.id")
    amount: int = Field(gt=0, le=100000000)


class InternalTransfer(InternalTransferBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)
    debitor: "Debitor" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})
    creditor: "Creditor" = Relationship(sa_relationship_kwargs={"lazy": "selectin"})


class InternalTransferCreate(SQLModel):
    debitor: DebitorBase
    creditor: CreditorBase
    amount: int


class InternalTransferPublic(InternalTransferBase):
    id: uuid.UUID
    debitor: DebitorPublic
    creditor: CreditorPublic
    amount: int


class InternalTransfersList(SQLModel):
    items: list[InternalTransferPublic]
    count: int
    total: int


class InternalTransferFilter(BaseFilter):
    debitor: Optional[DebitorFilter] = None
    creditor: Optional[CreditorFilter] = None
    amount: str | None = None
