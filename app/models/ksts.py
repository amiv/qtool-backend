import uuid
from datetime import datetime
from typing import Optional

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel

from app.models.filter import BaseFilter


class KstBase(SQLModel):
    kst_number: str = Field(max_length=4)
    name_de: str = Field(max_length=100)
    name_en: str = Field(max_length=100)
    owner: str = Field(max_length=30)  # TODO: change to user_id foreign key
    active: bool = Field(default=True)
    budget_plus: int = Field()
    budget_minus: int = Field()


class Kst(KstBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)


class KstPublic(KstBase):
    id: uuid.UUID
    budget_plus: int
    budget_minus: int


class KstsPublic(SQLModel):
    items: list[KstPublic] = []
    count: int
    total: int


class KstFilter(BaseFilter):
    kst_number: Optional[int] = None
    name_de: Optional[str] = None
    name_en: Optional[str] = None
    owner: Optional[str] = None
    active: Optional[bool] = None
    budget_plus: Optional[int] = None
    budget_minus: Optional[int] = None

    class Constants:
        model = Kst
        search_field_name = "search"
