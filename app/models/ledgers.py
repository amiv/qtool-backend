import uuid
from datetime import datetime
from typing import Optional

import uuid6
from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import EmailStr
from sqlmodel import Field, Relationship, SQLModel

from app.models.filter import BaseFilter


class LedgerBase(SQLModel):
    accountnumber: str = Field(max_length=4)
    name_de: str = Field(max_length=100)
    name_en: str = Field(max_length=100)
    visibility: int = Field(default=0)


class Ledger(LedgerBase, table=True):
    id: uuid.UUID = Field(default_factory=uuid6.uuid7, primary_key=True)
    time_create: datetime = Field(default_factory=datetime.now)
    time_modified: datetime = Field(default_factory=datetime.now)


class LedgerPublic(LedgerBase):
    id: uuid.UUID = Field()


class LedgersPublic(SQLModel):
    items: list[LedgerPublic] = []
    count: int
    total: int


class LedgerFilter(BaseFilter):
    accountnumber: Optional[int] = None
    name_de: Optional[str] = None
    name_en: Optional[str] = None
    search: Optional[str] = None
    visibility: Optional[int] = None

    class Constants:
        model = Ledger
        search_field_name = "search"
