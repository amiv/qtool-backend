"""debitor migration

Revision ID: 73b455b3ac76
Revises: ccf374357720
Create Date: 2025-02-15 20:48:57.989210
"""

import sqlalchemy as sa
import sqlmodel.sql.sqltypes
from alembic import op
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = "73b455b3ac76"
down_revision = "ccf374357720"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("debitor", sa.Column("accounting_year", sa.Integer(), nullable=True))
    op.add_column(
        "debitor",
        sa.Column(
            "q_check",
            sa.Enum("open", "accepted", "rejected", name="q_state"),
            nullable=True,
        ),
    )
    op.add_column(
        "debitor", sa.Column("q_check_timestamp", sa.DateTime(), nullable=True)
    )
    op.add_column(
        "debitor",
        sa.Column(
            "comment", sqlmodel.sql.sqltypes.AutoString(length=255), nullable=True
        ),
    )
    op.add_column(
        "debitor",
        sa.Column(
            "q_comment", sqlmodel.sql.sqltypes.AutoString(length=255), nullable=True
        ),
    )
    op.add_column(
        "debitor",
        sa.Column("name", sqlmodel.sql.sqltypes.AutoString(length=255), nullable=True),
    )
    op.drop_column("debitor", "mwst")


def downgrade():
    op.add_column("debitor", sa.Column("mwst", mysql.VARCHAR(length=30), nullable=True))
    op.drop_column("debitor", "name")
    op.drop_column("debitor", "q_comment")
    op.drop_column("debitor", "comment")
    op.drop_column("debitor", "q_check_timestamp")
    op.drop_column("debitor", "q_check")
    op.drop_column("debitor", "accounting_year")
