"""invoice and item migration

Revision ID: 83b455b3ac76
Revises: 73b455b3ac76
Create Date: 2025-02-15 20:48:57.989210
"""

import sqlalchemy as sa
import sqlmodel.sql.sqltypes
from alembic import op
from sqlalchemy.dialects import mysql

mwsttype = sa.Enum("INKL", "EXKL", name="mwsttype", native_enum=False)

# revision identifiers, used by Alembic.
revision = "83b455b3ac76"
down_revision = "73b455b3ac76"
branch_labels = None
depends_on = None


def upgrade():
    # Changes to the invoice table
    op.add_column("invoice", sa.Column("debitor_id", sa.Uuid(), nullable=True))
    op.add_column("invoice", sa.Column("invoice_date", sa.DateTime(), nullable=True))
    op.add_column(
        "invoice",
        sa.Column(
            "text_comment",
            sqlmodel.sql.sqltypes.AutoString(length=1000),
            nullable=True,
        ),
    )
    op.add_column(
        "invoice",
        sa.Column("mwst_type", mwsttype, nullable=True, server_default="INKL"),
    )
    op.add_column(
        "invoice",
        sa.Column(
            "our_reference",
            sqlmodel.sql.sqltypes.AutoString(length=255),
            nullable=True,
        ),
    )
    op.add_column(
        "invoice",
        sa.Column(
            "your_reference",
            sqlmodel.sql.sqltypes.AutoString(length=255),
            nullable=True,
        ),
    )
    # op.drop_constraint("invoice_ibfk_2", "invoice", type_="foreignkey")
    op.create_foreign_key(None, "invoice", "debitor", ["debitor_id"], ["id"])
    op.drop_column("invoice", "creator_id")
    op.drop_column("invoice", "q_check_timestamp")
    op.drop_column("invoice", "time_create")
    op.drop_column("invoice", "q_check")
    op.drop_column("invoice", "time_modified")

    # Changes to the item table
    op.add_column("item", sa.Column("mwst", sa.Float(), nullable=True))
    op.add_column(
        "item",
        sa.Column(
            "mwst_type", sqlmodel.sql.sqltypes.AutoString(length=255), nullable=True
        ),
    )
    op.add_column("item", sa.Column("mwst_published", sa.Float(), nullable=True))

    # Changes to the itemstoinvoice table
    op.add_column("itemstoinvoice", sa.Column("count", sa.Float(), nullable=True))
    # op.drop_constraint("itemstoinvoice_ibfk_1", "itemstoinvoice", type_="foreignkey")
    op.drop_column("itemstoinvoice", "debitor_id")


def downgrade():
    # Revert changes to the itemstoinvoice table
    op.add_column(
        "itemstoinvoice", sa.Column("debitor_id", mysql.CHAR(length=32), nullable=True)
    )
    op.create_foreign_key(
        "itemstoinvoice_ibfk_1", "itemstoinvoice", "debitor", ["debitor_id"], ["id"]
    )
    op.drop_column("itemstoinvoice", "count")

    # Revert changes to the item table
    op.drop_column("item", "mwst_published")
    op.drop_column("item", "mwst_type")
    op.drop_column("item", "mwst")

    # Revert changes to the invoice table
    op.add_column(
        "invoice", sa.Column("time_modified", mysql.DATETIME(), nullable=True)
    )
    op.add_column(
        "invoice",
        sa.Column("q_check", mysql.ENUM("open", "accepted", "rejected"), nullable=True),
    )
    op.add_column("invoice", sa.Column("time_create", mysql.DATETIME(), nullable=True))
    op.add_column(
        "invoice", sa.Column("q_check_timestamp", mysql.DATETIME(), nullable=True)
    )
    op.add_column(
        "invoice", sa.Column("creator_id", mysql.VARCHAR(length=32), nullable=True)
    )
    op.drop_constraint(None, "invoice", type_="foreignkey")
    op.create_foreign_key("invoice_ibfk_2", "invoice", "dbuser", ["creator_id"], ["id"])
    op.drop_column("invoice", "your_reference")
    op.drop_column("invoice", "our_reference")
    op.drop_column("invoice", "mwst_type")
    op.drop_column("invoice", "text_comment")
    op.drop_column("invoice", "invoice_date")
    op.drop_column("invoice", "debitor_id")
