from fastapi import HTTPException

from app.model import AllModelsCreate, AllModelsPublic
from app.models.Users import User


def q_field(
    user,
    fieldname,
    base_obj: AllModelsCreate | AllModelsPublic,
    update_obj: AllModelsCreate | AllModelsPublic | None = None,
    default=None,
):
    """
    This function ensures that the given field can only be updated if the user is a quaestor.
    """
    path = fieldname.split(".")
    # print(user)
    if user.role == "quaestor":
        return True
    base = base_obj
    for p in path[:-1]:
        base = getattr(base, p)
    update_base = update_obj
    if update_base is not None:
        for p in path[:-1]:
            update_base = getattr(update_base, p)

    if hasattr(base, fieldname):
        attr = getattr(base, fieldname)
        print("fieldname", fieldname, "attr", attr)

        if attr is not None:
            if update_obj is not None:
                updated_attr = getattr(update_base, fieldname)
                if attr != updated_attr:
                    raise HTTPException(
                        status_code=403,
                        detail=f"Field {fieldname} is not allowed to be updated by the user.",
                    )
                setattr(base, fieldname, updated_attr)
                return True
            if default is not None:
                setattr(base, fieldname, default)
                return True
            if attr == "":
                return True
            raise HTTPException(
                status_code=403,
                detail=f"Field {fieldname} is not allowed to be updated by the user.",
            )


def is_own_object(user: User, obj, field_name="creator"):
    """
    This function ensures that the given object is owned by the user.
    """
    if user.role == "quaestor":
        return True
    if obj.user_id != user.id:
        raise HTTPException(403, "You are not allowed to access this object.")
    return True


def lock_field(
    fieldname,
    base_obj: AllModelsCreate | AllModelsPublic,
    update_obj: AllModelsCreate | AllModelsPublic | None = None,
    default=None,
):
    """
    This function ensures that the given field can not be updated ever. It is locked.
    """
    path = fieldname.split(".")
    base = base_obj
    for p in path[:-1]:
        base = getattr(base, p)
    update_base = update_obj
    if update_base is not None:
        for p in path[:-1]:
            update_base = getattr(update_base, p)

    if hasattr(base, fieldname):
        attr = getattr(base, fieldname)
        print("fieldname", fieldname, "attr", attr)

        if attr is not None:
            if update_obj is not None:
                updated_attr = getattr(update_base, fieldname)
                setattr(base, fieldname, updated_attr)
                return True
            if default is not None:
                setattr(base, fieldname, default)
                return True
            if attr == "":
                return True
            raise HTTPException(403, f"Field {fieldname} is locked.")
    return True
