# pydantic ignore
import secrets
import warnings
from typing import Annotated, Any, Literal

from minio import Minio
from pydantic import (
    AnyUrl,
    BeforeValidator,
    HttpUrl,
    MariaDBDsn,
    PostgresDsn,
    computed_field,
    model_validator,
)
from pydantic_core import MultiHostUrl
from pydantic_settings import BaseSettings, SettingsConfigDict
from sqlmodel import Session, create_engine, select
from typing_extensions import Self


def parse_cors(v: Any) -> list[str] | str:
    if isinstance(v, str) and not v.startswith("["):
        return [i.strip() for i in v.split(",")]
    elif isinstance(v, list | str):
        return v
    raise ValueError(v)


class Settings(BaseSettings):
    model_config = SettingsConfigDict(
        env_file="./.env",
        env_ignore_empty=True,
        extra="ignore",
    )
    API_V1_STR: str = "/api"
    SECRET_KEY: str = secrets.token_urlsafe(32)
    # 60 minutes * 24 hours * 8 days = 8 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8
    FRONTEND_HOST: str = "http://localhost:5173"
    ENVIRONMENT: Literal["local", "staging", "production"] = "local"

    BACKEND_CORS_ORIGINS: Annotated[list[AnyUrl] | str, BeforeValidator(parse_cors)] = (
        []
    )
    OAUTH_CLIENT_ID: str = "Local Tool"
    RIGHTS_TREASURERS: str = "treasurers"
    RIGHTS_AUDITORS: str = "auditors"

    @computed_field  # type: ignore[prop-decorator]
    @property
    def all_cors_origins(self) -> list[str]:
        return [str(origin).rstrip("/") for origin in self.BACKEND_CORS_ORIGINS] + [
            self.FRONTEND_HOST,
            "api-qtool2-dev.amiv.ethz.ch",
            "api-qtool2-staging.amiv.ethz.ch",
            "api-qtool2.amiv.ethz.ch",
            "qtool2-dev.amiv.ethz.ch",
            "qtool2-staging.amiv.ethz.ch",
            "qtool2.amiv.ethz.ch",
            "api-qtool.amiv.ethz.ch",
            "qtool.amiv.ethz.ch",
        ]

    PROJECT_NAME: str
    SENTRY_DSN: HttpUrl | None = None
    SQL_SERVER: str
    SQL_PORT: int = 3306
    SQL_USER: str
    SQL_PASSWORD: str = ""
    SQL_DB: str = "app"
    """ @computed_field  # type: ignore[prop-decorator]
    @property
    def SQLALCHEMY_DATABASE_URI(self) -> MariaDBDsn:
        return MultiHostUrl.build(
            scheme="mysql+mariadbconnector",
            username=self.SQL_USER,
            password=self.SQL_PASSWORD,
            host=self.SQL_SERVER,
            port=self.SQL_PORT,
            path=f"/{self.SQL_DB}",
        )
     """
    SQLALCHEMY_DATABASE_URI: str = "mariadb+mariadbconnector://app:qtool@db:3306/app"
    SMTP_SERVER: str | None = None
    SMTP_PORT: int = 587
    SMTP_USERNAME: str | None = None
    SMTP_PASSWORD: str | None = None
    SMTP_TIMEOUT: int = 30
    # TODO: update type to EmailStr when sqlmodel supports it
    API_MAIL_ADDRESS: str = "qtool2-dev.amiv.ethz.ch"
    API_MAIL_NAME: str = "QTool2-dev"
    REPLY_TO: str = "quaestor@amiv.ethz.ch"
    API_MAIL_SUBJECT_PREFIX: str = "[qtool2-dev] "
    AMIV_API_URL: str = "api-dev.amiv.ethz.ch"
    TESTING: bool = True
    test_mails: list[dict[str, Any]] = []

    def _check_default_secret(self, var_name: str, value: str | None) -> None:
        if value == "changethis":
            message = (
                f'The value of {var_name} is "changethis", '
                "for security, please change it, at least for deployments."
            )
            if self.ENVIRONMENT == "local":
                warnings.warn(message, stacklevel=1)
            else:
                raise ValueError(message)

    @model_validator(mode="after")
    def _enforce_non_default_secrets(self) -> Self:
        self._check_default_secret("SECRET_KEY", self.SECRET_KEY)

        return self

    MINIO_ENDPOINT: str = "172.23.0.3:9000"  # Replace with your MinIO server URL
    MINIO_ACCESS_KEY: str = "minio"  # Replace with your MinIO access key
    MINIO_SECRET_KEY: str = "minio123"  # Replace with your MinIO secret key
    MINIO_BUCKET_NAME: str = "my-bucket"
    MINIO_SECURE: bool = False  # Set to True if using HTTPS


settings = Settings()  # type: ignore

engine = create_engine(settings.SQLALCHEMY_DATABASE_URI)

client: Minio = Minio(
    settings.MINIO_ENDPOINT,
    access_key=settings.MINIO_ACCESS_KEY,
    secret_key=settings.MINIO_SECRET_KEY,
    secure=settings.MINIO_SECURE,
)


def configure_cors():
    import boto3
    from botocore.client import Config
    from botocore.exceptions import ClientError

    options = {
        "endpoint_url": "https://" + settings.MINIO_ENDPOINT,
        "aws_access_key_id": settings.MINIO_ACCESS_KEY,
        "aws_secret_access_key": settings.MINIO_SECRET_KEY,
        "config": Config(signature_version="s3v4"),
        "region_name": "vis-is-great-1",
    }
    s3 = boto3.resource("s3", **options)
    s3_client = boto3.client("s3", **options)
    s3_bucket_cors = s3.BucketCors(settings.MINIO_BUCKET_NAME)
    try:
        s3_bucket_cors.put(
            CORSConfiguration={
                "CORSRules": [
                    {
                        "AllowedOrigins": [
                            (
                                "https://" + domain
                                if not domain.startswith("http")
                                else domain
                            )
                            for domain in settings.all_cors_origins
                        ],
                        "AllowedHeaders": ["Range"],
                        "AllowedMethods": ["GET"],
                        "ExposeHeaders": ["Accept-Ranges"],
                        "MaxAgeSeconds": 3000,
                    }
                ]
            }
        )
        print("CORS configuration updated successfully")
        print(s3_client.get_bucket_cors(Bucket="amiv-test")["CORSRules"])
    except ClientError as e:
        if e.response["Error"]["Code"] == "NoSuchCORSConfiguration":
            print("No CORS configuration found, creating a new one.")
        else:
            print(e)


# configure_cors()
