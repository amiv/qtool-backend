FROM python:3.13

ENV PYTHONUNBUFFERED=1

WORKDIR /app/

COPY ./requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt



ENV PYTHONPATH=/app

COPY dropDB.py /app/
COPY ./alembic.ini /app/
COPY ./entrypoint.sh /app/
RUN chmod +x /app/entrypoint.sh
COPY ./app /app/app
COPY .env /app/.env
COPY .env .env
EXPOSE 8000
CMD ["/app/entrypoint.sh"]
#test
